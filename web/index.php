<?php

// profiling with xhprof
// include_once "../debug/xhprof/include.php";

// Отключаем кеширование в шаблонизаторе
define("TWIG_CACHE_DISABLED", true);

$microtime = microtime(true);

require_once "../App/init.php";

try
{
	(new \App\Application())->start();
}
catch (Exception $e)
{
	print "<span style=\"color:red\">{$e->getMessage()}</span>";
}

var_dump(microtime(true) - $microtime);