-- phpMyAdmin SQL Dump
-- version 4.2.0-dev
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 12, 2014 at 06:43 PM
-- Server version: 5.5.35-33.0-log
-- PHP Version: 5.5.9-1+sury.org~saucy+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Table structure for table `p_CatalogProduct`
--

DROP TABLE IF EXISTS `p_CatalogProduct`;
CREATE TABLE `p_CatalogProduct` (
  `id` int(11) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `p_CatalogProductSection`
--

DROP TABLE IF EXISTS `p_CatalogProductSection`;
CREATE TABLE `p_CatalogProductSection` (
  `sectionId` int(11) NOT NULL,
  `productId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `p_CatalogSections`
--

DROP TABLE IF EXISTS `p_CatalogSections`;
CREATE TABLE `p_CatalogSections` (
  `id` int(11) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `code` varchar(150) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `leftKey` int(11) NOT NULL DEFAULT '0',
  `rightKey` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `p_Options`
--

DROP TABLE IF EXISTS `p_Options`;
CREATE TABLE `p_Options` (
  `code` varchar(50) NOT NULL,
  `description` varchar(150) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `p_User`
--

DROP TABLE IF EXISTS `p_User`;
CREATE TABLE `p_User` (
  `id` int(11) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `email` varchar(255) NOT NULL COMMENT 'length http://tools.ietf.org/html/rfc5321#section-4.5.3',
  `salt` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `secondName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `sex` enum('male','female') DEFAULT NULL,
  `created_at` datetime NOT NULL COMMENT 'Дата регистрации',
  `update_at` datetime NOT NULL COMMENT 'Дата последнего обновления',
  `last_authorized_at` datetime NOT NULL COMMENT 'Дата последней успешной авторизации'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `p_UserGroup`
--

DROP TABLE IF EXISTS `p_UserGroup`;
CREATE TABLE `p_UserGroup` (
  `id` int(11) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `code` varchar(20) NOT NULL,
  `adminAccess` enum('Y','N') NOT NULL DEFAULT 'N',
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `p_UserGroupMapping`
--

DROP TABLE IF EXISTS `p_UserGroupMapping`;
CREATE TABLE `p_UserGroupMapping` (
  `id` int(11) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `userId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p_CatalogProduct`
--
ALTER TABLE `p_CatalogProduct`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_CatalogProductSection`
--
ALTER TABLE `p_CatalogProductSection`
ADD UNIQUE KEY `productSection` (`sectionId`,`productId`);

--
-- Indexes for table `p_CatalogSections`
--
ALTER TABLE `p_CatalogSections`
ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `code` (`code`), ADD KEY `idx_nested_sets` (`leftKey`,`rightKey`,`level`);

--
-- Indexes for table `p_Options`
--
ALTER TABLE `p_Options`
ADD PRIMARY KEY (`code`);

--
-- Indexes for table `p_User`
--
ALTER TABLE `p_User`
ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `p_UserGroup`
--
ALTER TABLE `p_UserGroup`
ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `p_UserGroupMapping`
--
ALTER TABLE `p_UserGroupMapping`
ADD PRIMARY KEY (`id`), ADD KEY `userId` (`userId`), ADD KEY `groupId` (`groupId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p_CatalogProduct`
--
ALTER TABLE `p_CatalogProduct`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `p_CatalogSections`
--
ALTER TABLE `p_CatalogSections`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `p_User`
--
ALTER TABLE `p_User`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `p_UserGroup`
--
ALTER TABLE `p_UserGroup`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `p_UserGroupMapping`
--
ALTER TABLE `p_UserGroupMapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;