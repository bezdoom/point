<?php

// profiling with xhprof
// include_once "../debug/xhprof/include.php";

// Отключаем кеширование в шаблонизаторе
define("TWIG_CACHE_DISABLED", true);

$microtime = microtime(true);

require_once "../AdminApp/init.php";

try
{
	(new \AdminApp\Application())->start();
}
catch (Exception $e)
{
	print "<span style=\"color:red\">{$e->getMessage()} :: {$e->getFile()} :: {$e->getLine()}</span>";
}

// var_dump(microtime(true) - $microtime);