<?php

namespace Kernel;

abstract class Dictionary implements \ArrayAccess, \Countable, \Iterator
{
	protected $collection = null;

	public function __construct(array $collection)
	{
		$this->collection = $collection;
	}

	public function count()
	{
		return count($this->collection);
	}

	public function offsetSet($offset, $value)
	{
		if (is_null($offset))
			$this->collection[] = $value;
		else
			$this->collection[$offset] = $value;
	}

	public function offsetExists($offset)
	{
		return isset($this->collection[$offset]);
	}

	public function offsetUnset($offset)
	{
		unset($this->collection[$offset]);
	}

	public function offsetGet($offset)
	{
		return isset($this->collection[$offset]) ? $this->collection[$offset] : null;
	}

	public function rewind()
	{
		return reset($this->collection);
	}

	public function current()
	{
		return current($this->collection);
	}

	public function key()
	{
		return key($this->collection);
	}

	public function next()
	{
		return next($this->collection);
	}

	public function valid()
	{
		return key($this->collection) !== null;
	}

	public function getCollection()
	{
		return $this->collection;
	}

	public function random()
	{
		$keys = array_keys($this->collection);
		return $this->collection[$keys[rand(0, count($keys) - 1)]];
	}

	/**
	 * @return bool
	 */
	public function isEmpty()
	{
		return $this->count() <= 0;
	}
}