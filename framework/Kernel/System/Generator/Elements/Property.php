<?php

namespace Kernel\System\Generator\Elements;

use Kernel\System\Generator\ElementResult;

class Property
{
	protected $modification = "protected";

	protected $propertyName = null;

	/**
	 * @var \Kernel\System\Generator\ElementResult
	 */
	protected $value = null;

	public function __construct($name, ElementResult $value = null, $modification = "protected")
	{
		$this->propertyName = $name;
		$this->modification = $modification;
		$this->value = $value;
	}

	protected function getValue()
	{
		if (!$this->value)
			return "null";

		return $this->value->getString();
	}

	public function getString()
	{
		return "\t{$this->modification} \${$this->propertyName} = {$this->getValue()};";
	}
}