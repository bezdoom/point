<?php

namespace Kernel\System\Generator;

interface ElementResult
{
	function getString();
}