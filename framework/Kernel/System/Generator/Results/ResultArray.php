<?php

namespace Kernel\System\Generator\Results;

use Kernel\System\Generator\ElementResult;

class ResultArray implements ElementResult
{
	protected $source = null;

	public function __construct(array $source)
	{
		$this->source = $source;
	}

	protected function getArrayAsString(array $arr)
	{
		$result = array();
		foreach ( $arr as $key => $value )
		{
			if (is_array($value))
				$result[] = "\t\t\t\"{$key}\" => " . $this->getArrayAsString($value);
			elseif 	(is_numeric($value))
				$result[] = "\t\t\t\"{$key}\" => {$value}";
			else
				$result[] = "\t\t\t\"{$key}\" => \"{$value}\"";
		}

		return "array(\n" . implode("\n", $result) . ")";
	}

	public function getString()
	{
		return $this->getArrayAsString($this->source);
	}
}