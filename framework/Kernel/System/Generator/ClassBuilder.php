<?php

namespace Kernel\System\Generator;

use Kernel\System\Generator\Elements\Property;

class ClassBuilder
{
	protected $className = null;

	protected $namespace = array();

	/**
	 * @var Property[]
	 */
	protected $properties = array();

	protected $abstract = false;

	protected $extends = false;

	function __construct($className, $extends, $abstract = false, array $namespace = array())
	{
		$this->className = $className;
		$this->extends = $extends;
		$this->abstract = $abstract;
		$this->namespace = $namespace;
	}

	protected function getAbstract()
	{
		return  $this->abstract ? "abstract " : "";
	}

	public function addProperty(Property $prop)
	{
		$this->properties[] = $prop;
	}

	public function isNamespace()
	{
		return !empty($this->namespace);
	}

	public function getClassName()
	{
		return $this->className;
	}

	public function getNamespace()
	{
		if (!$this->isNamespace())
			return "";

		return "namespace " . implode("\\", $this->namespace) . ";\n\n";
	}

	public function getExtends()
	{
		if (!$this->extends)
			return "";

		return " extends {$this->extends}";;
	}

	public function getProperties()
	{
		$result = array();
		foreach ( $this->properties as $prop )
			$result[] = $prop->getString();

		return implode("\n\n", $result);
	}

	public function getString()
	{
		return "<?php\n\n{$this->getNamespace()}{$this->getAbstract()}class {$this->getClassName()}{$this->getExtends()} \n{\n{$this->getProperties()}\n}";
	}

	public function __toString()
	{
		return $this->getString();
	}
}