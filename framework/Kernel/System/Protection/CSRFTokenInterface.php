<?php

namespace Kernel\System\Protection;

interface CSRFTokenInterface
{
    /**
     * @return string
     */
    public static function GetToken();

    /**
     * @param string $token
     * @return bool
     */
    public static function Validate($token);

    /**
     * @return string
     */
    public static function Generate();
}