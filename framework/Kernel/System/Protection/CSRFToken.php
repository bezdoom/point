<?php

namespace Kernel\System\Protection;

use Helper\Encrypt;

class CSRFToken implements CSRFTokenInterface
{
    private static $instance = null;

    private static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }

    private function __clone() {}
    private function __construct() {}

    /**
     * @return string
     */
    private function makeToken()
    {
        return Encrypt::hmac_sha1(POINT_CSRF_MIXIN, md5(uniqid(rand(), true)));
    }

    /**
     * @return void
     */
    private function setSessionToken()
    {
        $_SESSION[POINT_ROOT_SESSION]["PROTECTION"]["CSRF_TOKEN"] = $this->makeToken();
    }

    /**
     * @return bool
     */
    private function checkTokenExist()
    {
        return isset($_SESSION[POINT_ROOT_SESSION]["PROTECTION"]["CSRF_TOKEN"]);
    }

    /**
     * @return string
     */
    private function getCurrentToken()
    {
        if (!$this->checkTokenExist())
            $this->setSessionToken();

        return $_SESSION[POINT_ROOT_SESSION]["PROTECTION"]["CSRF_TOKEN"];
    }

    /**
     * @return string
     */
    public static function GetToken()
    {
        return self::getInstance()->getCurrentToken();
    }

    /**
     * @param $token
     * @return bool
     */
    public static function Validate($token)
    {
        return self::getInstance()->getCurrentToken() == $token;
    }

    /**
     * @return string
     */
    public static function Generate()
    {
        self::getInstance()->setSessionToken();
        return self::getInstance()->getCurrentToken();
    }
}