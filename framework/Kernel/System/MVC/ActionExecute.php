<?php

namespace Kernel\System\MVC;

use Helper\DocParser;
use Main\Application;
use Main\Controller;
use Main\User\UserIdentify;

class ActionExecute
{
	const DOC_FUNCTION_ACTION = "Action";
	const DOC_FUNCTION_DENY_FROM_ALL = "DenyFromAll";

	/**
	 * @var \Main\Controller
	 */
	protected $controller = null;
	protected $actionName = null;
	protected $internal = null;
	/**
	 * @var \Main\Application
	 */
	protected $application = null;

	/**
	 * @var \ReflectionMethod
	 */
	protected $reflection = null;
	/**
	 * @var DocParser
	 */
	protected $docParser = null;

	public function __construct(Controller &$controller, Application &$app, $actionName, $internal = false)
	{
		$this->application = $app;
		$this->controller = $controller;
		$this->actionName = $actionName;
		$this->internal = $internal;
	}

	/**
	 * @return bool
	 */
	protected function isActionExists()
	{
		return method_exists($this->controller, $this->actionName);
	}

	/**
	 * @return \ReflectionMethod
	 */
	protected function getReflection()
	{
		if (is_null($this->reflection))
			$this->reflection = new \ReflectionMethod($this->controller, $this->actionName);

		return $this->reflection;
	}

	/**
	 * @return DocParser|null
	 */
	protected function getDocParser()
	{
		if (is_null($this->docParser))
			$this->docParser = new DocParser($this->getReflection()->getDocComment());

		return $this->docParser;
	}

	/**
	 * @param string $function
	 *
	 * @return array
	 */
	protected function getDocFunction($function)
	{
		return $this->getDocParser()->getFunctionValue($function);
	}

	/**
	 * @return bool
	 */
	protected function isValidAction()
	{
		return !is_null($this->getDocFunction($this::DOC_FUNCTION_ACTION));
	}

	/**
	 * @return string
	 */
	protected function getCurrentRequestMethod()
	{
		return strtolower($_SERVER["REQUEST_METHOD"]);
	}

	/**
	 * @return array
	 */
	protected function getAllowRequestMethods()
	{
		$access = $this->getDocFunction($this::DOC_FUNCTION_ACTION);

		if (is_null($access))
			return null;

		if (empty($access))
			$access[] = $this->getCurrentRequestMethod();

		foreach($access as $key => $value)
			$access[$key] = strtolower($value);

		return $access;
	}

	/**
	 * @return array
	 */
	protected function getAllowUserGroups()
	{
		return $this->getDocFunction($this::DOC_FUNCTION_DENY_FROM_ALL);
	}

	/**
	 * @return bool
	 */
	protected function isValidRequestMethod()
	{
		return in_array($this->getCurrentRequestMethod(), $this->getAllowRequestMethods());
	}

	/**
	 * @return array
	 */
	protected function getReflectionPassion()
	{
		$passion = array();
		$arguments = $this->application->getRoutingMap()->getCurrentRoute()->getArguments();

		foreach($this->reflection->getParameters() as $param)
		{
			if (isset($arguments[$param->getName()]))
				$passion[] = $arguments[$param->getName()];
			else
				$passion[] = $param->getDefaultValue();
		}

		return $passion;
	}

	/**
	 * @return bool
	 */
	protected function isAllowAction()
	{
		if (is_null($allowUserGroups = $this->getAllowUserGroups()))
			return true;

		$userIdentify = UserIdentify::getInstance();

		if ($userIdentify->isAdmin())
			return true;

		foreach ($userIdentify->getGroups() as $group)
		{
			if (in_array($group->code, $allowUserGroups))
				return true;
		}

		return false;
	}

	/**
	 * @param $message
	 *
	 * @throws ActionExecuteException
	 */
	protected function throwInternal($message)
	{
		if ($this->internal)
			throw new ActionExecuteException($message);
	}

	/**
	 * @return mixed
	 */
	public function reflectionIt()
	{
		if (!$this->isActionExists() or !$this->isValidAction())
		{
			$this->throwInternal("Action {$this->actionName} does not exist in controller");
			return $this->application->executeNotFound();
		}

		if (!$this->isValidRequestMethod())
		{
			$this->throwInternal("Action {$this->actionName} does not work with current request method");
			return $this->application->executeNotFound();
		}

		if (!$this->isAllowAction())
		{
			$this->application->handlerOnDenyAction();
			$this->throwInternal("Action {$this->actionName} does not have access for current user group");
			return $this->application->executeNotFound();
		}

		return $this->getReflection()->invokeArgs($this->controller, $this->getReflectionPassion());
	}
}