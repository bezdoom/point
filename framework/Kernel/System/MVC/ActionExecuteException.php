<?php

namespace Kernel\System\MVC;

use Main\Routing\NotFoundException;

class ActionExecuteException extends NotFoundException {}