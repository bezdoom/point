<?php

namespace Kernel\Admin;

interface AdminRenderInterface
{
    /**
     * @return string
     */
    public function Render();
}