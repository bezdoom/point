<?php

namespace Kernel\Admin\TableGrid;

use Helper\Storage;
use Helper\String;
use Kernel\Admin\AdminRenderInterface;

class Row implements AdminRenderInterface
{
    /**
     * @var Column[]
     */
    protected $columns = array();

    /**
     * @param $text
     * @return Column
     */
    public function &addColumn($text)
    {
        $this->columns[] = new Column($text);

        return $this->columns[count($this->columns) - 1];
    }

    /**
     * @param $text
     * @return HeadColumn
     */
    public function &addHeadColumn($text)
    {
        $this->columns[] = new HeadColumn($text);

        return $this->columns[count($this->columns) - 1];
    }

    /**
     * @return string
     */
    protected function getColumnsRender()
    {
        $result = String::STRING_EMPTY;

        foreach ($this->columns as $column)
            $result .= $column->Render();

        return $result;
    }

    /**
     * @return string
     */
    public function Render()
    {
        return Storage::getTwig()->render("_system/TableGrid/row.twig", array( "columns" => $this->getColumnsRender() ));
    }
}