<?php

namespace Kernel\Admin\TableGrid;

use Helper\Storage;
use Kernel\Admin\AdminRenderInterface;

class Column implements AdminRenderInterface
{
    protected $text = null;

    function __construct($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function Render()
    {
        return Storage::getTwig()->render("_system/TableGrid/column.twig", array( "text" => $this->text ));
    }
}