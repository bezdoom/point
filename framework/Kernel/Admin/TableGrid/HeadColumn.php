<?php

namespace Kernel\Admin\TableGrid;

use Helper\Storage;

class HeadColumn extends Column
{
    /**
     * @return string
     */
    public function Render()
    {
        return Storage::getTwig()->render("_system/TableGrid/headColumn.twig", array( "text" => $this->text ));
    }
}