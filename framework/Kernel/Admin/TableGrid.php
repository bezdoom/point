<?php

namespace Kernel\Admin;

use Helper\Storage;
use Helper\String;
use Kernel\Admin\TableGrid\Row;

class TableGrid implements AdminRenderInterface
{
    /**
     * @var Row[]
     */
    protected $rows = array();

    /**
     * @return Row
     */
    public function &addRow()
    {
        $this->rows[] = new Row();

        return $this->rows[count($this->rows) - 1];
    }

    /**
     * @return string
     */
    protected function getRowsRender()
    {
        $result = String::STRING_EMPTY;

        foreach ($this->rows as $row)
            $result .= $row->Render();

        return $result;
    }

    /**
     * @return string
     */
    public function Render()
    {
        return Storage::getTwig()->render("_system/TableGrid/table.twig", array( "rows" => $this->getRowsRender() ));
    }
}