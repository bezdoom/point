<?php

namespace Kernel\Admin;

use Helper\Storage;
use Helper\String;
use Main\DBLayer\baseLayer;
use Main\DBLayer\Finder;
use Main\DBLayer\Logic\LogicInterface;

class ModelTableGrid extends TableGrid
{
    /**
     * @var \Main\DBLayer\baseLayer
     */
    protected $model = null;
    /**
     * @var \Main\DBLayer\Logic\LogicInterface
     */
    protected $filter = null;

    protected $buttons = array();

    function __construct($model, LogicInterface $filter = null)
    {
        $this->modelName = $model;
        $this->model = new $model();
        $this->filter = $filter;

        if (!($this->model instanceof baseLayer))
            throw new \RuntimeException("Bad model");
    }

    /**
     * @return bool
     */
    protected function existButtons()
    {
        return !empty($this->buttons);
    }

    /**
     * @return \Main\DBLayer\ResultCollection
     */
    protected function getCollection()
    {
        $finder = Finder::Model($this->modelName);

        if (!is_null($this->filter))
            $finder->where($this->filter);

        return $finder->find();
    }

    protected function buildHeadTable()
    {
        $metaModel = $this->model->getAdminMetaModel();

        $row =& $this->addRow();

        if ($this->existButtons())
            $row->addHeadColumn(String::STRING_EMPTY);

        foreach ($metaModel as $field)
            $row->addHeadColumn($field["TITLE"]);

        return $this;
    }

    /**
     * @param baseLayer $element
     * @return string
     */
    protected function getButtons(baseLayer &$element)
    {
        return Storage::getTwigString()->render('<a href="{{ href }}" class="btn btn-default btn-xs glyphicon-pencil"></button>', array(
            "href" => "/options/edit/?" . http_build_query(array("pk" => $element->getPrimaryValue()))
        ));
    }

    protected function buildTable()
    {
        /**
         * @var baseLayer[] $collection
         */
        $collection = $this->getCollection();

        foreach ($collection as $element)
        {
            $row =& $this->addRow();

            if ($this->existButtons())
                $row->addColumn($this->getButtons($element));

            foreach ($element->getAdminMetaModel() as $dataField)
                $row->addColumn($dataField["VALUE"]);
        }

        return $this;
    }

    protected function buildTableGrid()
    {
        $this->buildHeadTable()->buildTable();
    }

    /**
     * @param array $buttons
     * @return $this
     */
    public function setButtons(array $buttons)
    {
        $this->buttons = $buttons;
        return $this;
    }

    /**
     * @return string
     */
    public function Render()
    {
        $this->buildTableGrid();

        return parent::Render();
    }
}