<?php

namespace Kernel\Admin;

use Helper\Storage;
use Helper\String;
use Kernel\Admin\ActionButtons\DefaultButton;

class ActionButtonsGroup implements AdminRenderInterface
{
    private static $instance = null;

    public static function &getInstance()
    {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }
    protected function __clone() {}
    protected function __construct() {}

    /**
     * @var DefaultButton[]
     */
    protected $buttons = array();

    /**
     * @param DefaultButton $button
     * @return $this
     */
    public function addButton(DefaultButton $button)
    {
        $this->buttons[] = $button;
        return $this;
    }

    /**
     * @return string
     */
    protected function getButtonsRender()
    {
        $text = String::STRING_EMPTY;

        foreach ($this->buttons as $button)
            $text .= $button->Render();

        return $text;
    }

    /**
     * @return string
     */
    public function Render()
    {
        if (empty($this->buttons))
            return String::STRING_EMPTY;

        return Storage::getTwig()->render('_system/ButtonsGroup/group.twig', array(
            "buttons" => $this->getButtonsRender()
        ));
    }
}