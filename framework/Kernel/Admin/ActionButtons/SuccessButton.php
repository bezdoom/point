<?php

namespace Kernel\Admin\ActionButtons;

class SuccessButton extends DefaultButton
{
    protected $class = "btn-success";
}