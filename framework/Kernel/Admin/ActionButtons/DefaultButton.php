<?php

namespace Kernel\Admin\ActionButtons;

use Helper\Storage;
use Kernel\Admin\AdminRenderInterface;

class DefaultButton implements AdminRenderInterface
{
    protected $href = null;
    protected $text = null;

    protected $class = "btn-default";

    function __construct($text, $href)
    {
        $this->text = $text;
        $this->href = $href;
    }

    /**
     * @return string
     */
    public function Render()
    {
        return Storage::getTwig()->render('_system/ButtonsGroup/button.twig', array(
            "class" => $this->class,
            "href" => $this->href,
            "text" => $this->text
        ));
    }
}