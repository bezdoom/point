<?php

namespace Kernel\Admin\ActionButtons;

class WarningButton extends DefaultButton
{
    protected $class = "btn-warning";
}