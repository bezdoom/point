<?php

namespace Kernel\Admin\ActionButtons;

class LinkButton extends DefaultButton
{
    protected $class = "btn-link";
}