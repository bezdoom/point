<?php

namespace Kernel\Admin\ActionButtons;

class PrimaryButton extends DefaultButton
{
    protected $class = "btn-primary";
}