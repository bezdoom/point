<?php

namespace Kernel\Admin\ActionButtons;

class DangerButton extends DefaultButton
{
    protected $class = "btn-danger";
}