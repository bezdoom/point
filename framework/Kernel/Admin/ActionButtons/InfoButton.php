<?php

namespace Kernel\Admin\ActionButtons;

class InfoButton extends DefaultButton
{
    protected $class = "btn-info";
}