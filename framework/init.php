<?php

if (version_compare(phpversion(), '5.5.0', '<'))
	die("Need PHP >= 5.5 (current ".phpversion().")");

header ( "Content-Type: text/html; charset=utf-8" );

session_start();

require_once "define.php";

if (!file_exists(TMP_TWIG_PATH))
	mkdir(TMP_TWIG_PATH, 0777, true);

require_once "autoloader.php";
require_once "functions.php";

/*
 * Register Vendors libraries
 */
// TWIG
(new AutoLoader("Twig", "../vendor/Twig-1.15.1/lib"))->setNamespaceSeparator("_")->register();
// SWIFT MAILER
require_once "../vendor/Swift-5.0.3/lib/swift_required.php";

/**
 * Register Point classes strict
 */
foreach (array(
	         "Helper" => "../framework",
	         "Kernel" => "../framework",
	         "Main" => "../framework",
             "Components" => "../framework"
         ) as $namespace => $includePath)
	(new AutoLoader($namespace, $includePath))->register();