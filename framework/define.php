<?php

if (!defined("DEFAULT_TITLE"))
    define("DEFAULT_TITLE", "Untitled");

if (!defined("POINT_ROOT_SESSION"))
    define("POINT_ROOT_SESSION", "POINT");

if (!defined("POINT_CSRF_MIXIN"))
    define("POINT_CSRF_MIXIN", md5("point-csrf-default"));

define("APC_AVAILABLE", extension_loaded("apc") and ini_get("apc.enabled"));

define("TMP_PATH", realpath(__DIR__ . "/../") . '/tmp');
define("TMP_TWIG_PATH", realpath(__DIR__ . "/../") . '/tmp/twig');