<?php

namespace Helper;

use Main\DBLayer\DBLayerException;
use Main\Models\Option;

class Options
{
    private static $instance = null;

    private function __clone() {}
    private function __consturct() {}

    /**
     * @var Option[]
     */
    private $storage = [];

    private static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }

    /**
     * @param string $code
     * @return Option
     */
    private function &_findOption($code)
    {
        if (array_key_exists($code, $this->storage))
            return $this->storage[$code];

        try
        {
            $this->storage[$code] = new Option($code);
        }
        catch (DBLayerException $e)
        {
            return null;
        }
        finally
        {
            return $this->storage[$code];
        }
    }

    /**
     * @param string $code
     * @param string $value
     */
    private function _saveOption($code, $value)
    {
        $option = $this->_findOption($code);

        if (!$option)
            $option = new Option();

        $option->code = $code;
        $option->value = $value;
        $option->Save();

        if (!array_key_exists($code, $this->storage))
            $this->storage[$code] = $option;
    }

    /**
     * @param string $code
     * @return mixed
     */
    public static function &get($code)
    {
        return self::getInstance()->_findOption($code)->getValue("value");
    }

    /**
     * @param string $code
     * @return mixed
     */
    public static function &description($code)
    {
        return self::getInstance()->_findOption($code)->getValue("description");
    }

    /**
     * @param string $code
     * @param string $value
     */
    public static function set($code, $value)
    {
        self::getInstance()->_saveOption($code, $value);
    }
}