<?php

namespace Helper;

/**
 * @static
 * @name ArrayHelper
 * @author Viktor Kulikov <bezdoom@gmail.com>
 *
 * Статический класс, помогающий в работе с массивами.
 */
class ArrayHelper
{
	const SORT_ASC = "ASC";
	const SORT_DESC = "DESC";

	/**
	 * @param array $original
	 * @param int $bagItemsCount
	 *
	 * @return array
	 *
	 * Раскладывает массив на несколько, помещая в каждый не более $bagItemsCount элементов
	 */
	public static function ExpandedInBags(array $original, $bagItemsCount = 2)
	{
		$iteration = 0;
		$bags = [];
		foreach($original as $key => $value)
		{
			if (!isset($bags[$iteration]))
				$bags[$iteration] = [];

			if (count($bags[$iteration]) >= $bagItemsCount)
			{
				$iteration++;
				$bags[$iteration] = [];
			}

			$bags[$iteration][$key] = $value;
		}

		return $bags;
	}

	/**
	 * @param array $original
	 * @param int $lines
	 *
	 * @return array
	 *
	 * Раскладывает массив в $lines массивов, помещая элементы по очереди в каждый
	 */
	public static function ExpendedInLines(array $original, $lines = 2)
	{
		$currentKey = 0;
		$linesArray = [];
		foreach($original as $key => $value)
		{
			if (!isset($linesArray[$currentKey]))
				$linesArray[$currentKey] = [];

			$linesArray[$currentKey][$key] = $value;

			$currentKey++;
			if ($currentKey > ($lines - 1))
				$currentKey = 0;
		}

		return $linesArray;
	}

	public static function Sort ($array, $on, $order = self::SORT_ASC)
	{
		$new_array = [];
		$sortable_array = [];

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case self::SORT_ASC:
					asort($sortable_array);
					break;
				case self::SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
}