<?php

namespace Helper;

use Main\View\Buffer;

class Storage
{
	const URI_STORAGE_KEY = "uri";
	const TWIG_STORAGE_KEY = "twig";
	const TWIG_STRING_STORAGE_KEY = "twig_string";

	protected static $instance;

	protected $storage = [];

	public function __construct ()
	{
		if ( self::$instance )
			throw new \RuntimeException('Нельзя создавать несколько хранилишь');
	}

	public static function &getInstance ()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	public static function &getS($key)
	{
		$storage = self::getInstance();
		return $storage->get($key);
	}

	public function &get($key)
	{
		if (!$key or !isset($this->storage[$key]))
			throw new \RuntimeException('Ошибка при получении значения');

		return $this->storage[$key];
	}

	public static function setS($key, &$value)
	{
		$storage = self::getInstance();
		$storage->set($key, $value);
	}

	public function set($key, &$value)
	{
		if (!$key)
			throw new \RuntimeException('Пустой ключ');
		$this->storage[$key] = $value;
	}

	public static function checkS ($key)
	{
		$storage = self::getInstance();
		return $storage->check($key);
	}

	/**
	 * @return Uri
	 */
	public static function &getCurrentUri()
	{
		if (!self::checkS(self::URI_STORAGE_KEY))
			self::setS(self::URI_STORAGE_KEY, new Uri());

		return self::getS(self::URI_STORAGE_KEY);
	}

	/**
	 * @return \Twig_Environment
	 */
	public static function &getTwig()
	{
		if (!self::checkS(self::TWIG_STORAGE_KEY))
		{
			$loader = new \Twig_Loader_Filesystem(Buffer::getInstance()->getEnvPath());

			$options = array();
			if (!defined("TWIG_CACHE_DISABLED") or TWIG_CACHE_DISABLED !== true)
				$options['cache'] = TMP_TWIG_PATH;

			$twig = new \Twig_Environment($loader, $options);

			self::setS(self::TWIG_STORAGE_KEY, $twig);
		}

		return self::getS(self::TWIG_STORAGE_KEY);
	}

	/**
	 * @return \Twig_Environment
	 */
	public static function &getTwigString()
	{
		if (!self::checkS(self::TWIG_STRING_STORAGE_KEY))
		{
			$loader = new \Twig_Loader_String();

			$options = array();
			if (!defined("TWIG_CACHE_DISABLED") or TWIG_CACHE_DISABLED !== true)
				$options['cache'] = TMP_TWIG_PATH;

			$twig = new \Twig_Environment($loader, $options);

			self::setS(self::TWIG_STRING_STORAGE_KEY, $twig);
		}

		return self::getS(self::TWIG_STRING_STORAGE_KEY);
	}

	public function check($key)
	{
		return isset($this->storage[$key]);
	}

}
