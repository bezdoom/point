<?php

namespace Helper;

class DocParser
{
	const REGEXP_FUNCTION = '#@([A-z0-9]+)\(([A-z0-9|, ]*)\)#is';

	protected $sourceDoc = null;
	protected $functions = null;

	public function __construct($source)
	{
		$this->sourceDoc = $source;

		if (!$this->sourceDoc)
			$this->functions = [];
	}

	/**
	 * @param string $paramsString
	 *
	 * @return array
	 */
	protected function parseParams($paramsString)
	{
		if (!trim($paramsString))
			return [];

		$array = explode(",", $paramsString);

		foreach ( $array as $key => $param )
			$array[$key] = trim($param);

		return $array;
	}

	protected function parseDoc()
	{
		$this->functions = [];

		$vars = array();
		if (preg_match_all(self::REGEXP_FUNCTION, $this->sourceDoc, $vars, PREG_SET_ORDER) <= 0)
			return;

		foreach($vars as $matchResult)
			$this->functions[$matchResult[1]] = $this->parseParams($matchResult[2]);
	}

	/**
	 * @param $functionName
	 *
	 * @return array
	 */
	public function getFunctionValue($functionName)
	{
		if (!$this->functions)
			$this->parseDoc();

		if (array_key_exists($functionName, $this->functions))
			return $this->functions[$functionName];

		return null;
	}
}