<?php

namespace Helper;

/**
 * @static
 * @name String
 * @author Viktor Kulikov <bezdoom@gmail.com>
 *
 * Статический класс, помогающий в работе со строками UTF
 */
class String
{
    /**
     * Кодировка для mbstring library
     */
    const MBSTRING_CHARSET = "UTF-8";
    /**
     * Константа с пустой строкой
     */
    const STRING_EMPTY = "";

    /**
     * @static
     * @param string $needle
     * @param string $string
     * @param string $charset
     * @return bool
     *
     * Проверяет на наличие вхождения $needle в строке $string
     */
    static public function Contains($needle, $string, $charset = self::MBSTRING_CHARSET)
    {
        return (mb_strpos($string, $needle, null, $charset) !== false);
    }

    /**
     * @static
     * @param string $needle
     * @param string $string
     * @param string $charset
     * @return bool
     *
     * Выполняет проверку, начинается ли строка $string с указанного вхождения $needle
     */
    static public function StartWith($needle, $string, $charset = self::MBSTRING_CHARSET)
    {
        return (mb_substr($string, 0, mb_strlen($needle, $charset), $charset) === $needle);
    }

    /**
     * @static
     * @param string $needle
     * @param string $string
     * @param string $charset
     * @return bool
     *
     * Выполняет проверку, заканчивается ли строка $string на указанное вхождение $needle
     */
    static public function EndWith($needle, $string, $charset = self::MBSTRING_CHARSET)
    {
        $needle_length = mb_strlen($needle, $charset);
        $total_length = mb_strlen($string, $charset);
        return (mb_substr($string, $total_length - $needle_length, $needle_length, $charset) === $needle);
    }

	/**
	 * @static
	 * @param string $needle
	 * @param string $string
	 * @param string $charset
	 * @return string
	 *
	 * Возвращает строку, вырезая из ее начала $needle
	 */
	static public function RemoveFirst($needle, $string, $charset = self::MBSTRING_CHARSET)
	{
		if(!static::StartWith($needle, $string, $charset))
			return $string;

		$needle_length = mb_strlen($needle, $charset);
		$total_length = mb_strlen($string, $charset);
		return mb_substr($string, $needle_length, $total_length - $needle_length, $charset);
	}

    /**
     * @static
     * @param string $string
     * @param string $charset
     * @return string
     *
     * Переворачивает строку $string
     */
    static public function Reverse($string, $charset = self::MBSTRING_CHARSET)
    {
        $output_string = "";
        $string_length = mb_strlen($string, $charset);

        for($i = $string_length - 1; $i >= 0; --$i)
            $output_string .= mb_substr($string, $i, 1, $charset);

        return $output_string;
    }

    /**
     * @static
     * @param string $string
     * @return string
     *
     * Обрезает лишние пробелы по краям с поддержкой многобайтовой кодировки (UTF-8)
     */
    static public function Trim($string)
    {
        return preg_replace("/(^\s+)|(\s+$)/us", "", $string);
    }

    /**
     * @static
     * @param string $string
     * @param string $charset
     * @return string
     *
     * Выполняет транслитераию строки с поддержкой многобайтовой кодировки
     */
    static public function Translate($string, $charset = self::MBSTRING_CHARSET)
    {
        static $rus, $lat;
        if ( is_null( $rus ) )
        {
            $rus = ['ё','ж','ц','ч','ш','щ','ю','я','Ё','Ж','Ц','Ч','Ш','Щ','Ю','Я'];
            $lat = ['yo','zh','tc','ch','sh','sh','yu','ya','YO','ZH','TC','CH','SH','SH','YU','YA'];

            $rusChars = "АБВГДЕЗИЙКЛМНОПРСТУФХЪЫЬЭабвгдезийклмнопрстуфхъыьэ";
            $latChars = "ABVGDEZIJKLMNOPRSTUFH_I_Eabvgdezijklmnoprstufh_i_e";
            $strLen = mb_strlen( $rusChars, $charset );
            for( $i = 0 ; $i < $strLen; $i++ )
            {
                $rus[] = mb_substr( $rusChars, $i, 1, $charset );
                $lat[] = mb_substr( $latChars, $i, 1, $charset );
            }
        }
        return str_replace( $rus, $lat, $string );
    }

    /**
     * @static
     * @param string $input
     * @param int $substring_length
     * @param bool $to_lower
     * @param string $charset
     * @return string
     *
     * Выполняет преобразования строки к корректному виду в URL
     */
    static public function Prepare2URL($input, $substring_length = 40, $to_lower = true, $charset = self::MBSTRING_CHARSET)
    {
        $search     = [' ', '.', ',', '/', '\\', '!', '@', '&', ":", ";", "%", "?", "<", ">", "$", "#", "^", "*", "(", ")", "`", "'", "\""];
        $replace    = ['-', '' , '' , '_', '_',  '' , '' , '',  "",  "",  "",  "",  "",  "",  "",  "",  "",  "",  "",  "",  "",  "",  ""];

        $result     = mb_substr( str_replace( $search, $replace, trim($input) ), 0, $substring_length, $charset );

        if ($to_lower === true)
            return mb_strtolower($result, $charset);
        else
            return $result;
    }
}