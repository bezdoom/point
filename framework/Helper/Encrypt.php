<?php

namespace Helper;

class Encrypt
{
    /**
     * @param $key
     * @param $data
     * @param string $hashfunc
     * @param int $blocksize
     * @return string
     */
    public static function hmac_sha1 ( $key, $data, $hashfunc = "sha1", $blocksize = 64 )
    {
        if ( strlen( $key ) > $blocksize )
            $key = pack( 'H*', $hashfunc( $key ) );

        $key = str_pad( $key, $blocksize, chr( 0x00 ) );
        $ipad = str_repeat( chr( 0x36 ), $blocksize );
        $opad = str_repeat( chr( 0x5c ), $blocksize );
        $hmac = pack(
            'H*', $hashfunc(
                ( $key ^ $opad ) . pack(
                    'H*', $hashfunc(
                        ( $key ^ $ipad ) . $data
                    )
                )
            )
        );
        return bin2hex( $hmac );
    }
}