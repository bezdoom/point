<?php

namespace Main\Cache;

interface ICache
{
	public function current();

	public function Save($valueID, $value);
	public function Load($valueID, $ttl = 3600);
	public function Delete($valueID);
	public function Clear();
}