<?php

namespace Main;

use Helper\Storage;
use Main\Context\HttpApplication;
use Main\Context\HttpRequest;
use Main\Context\HttpResponse;
use Main\DBLayer\ConnectionPool;
use Main\User\UserIdentify;

abstract class ContextLayer
{
	/**
	 * @var \Helper\Uri
	 */
	public $uri = null;
	/**
	 * @var \Twig_Environment
	 */
	public $twig = null;
	/**
	 * @var Context\HttpRequest
	 */
	public $request = null;
	/**
	 * @var Context\HttpResponse
	 */
	public $response = null;
	/**
	 * @var Context\HttpApplication
	 */
	public $application = null;
	/**
	 * @var User\UserIdentify
	 */
	public $user = null;
    /**
     * @var DBLayer\ConnectionPool
     */
    public $connection = null;

	function __construct()
	{
		$this->uri = Storage::getCurrentUri();
		$this->twig = Storage::getTwig();
		$this->request = HttpRequest::getInstance();
		$this->response = HttpResponse::getInstance();
		$this->application = HttpApplication::getInstance();
		$this->user = UserIdentify::getInstance();
        $this->connection = ConnectionPool::getConnection();
	}
}