<?php

namespace Main\View;

class Redirect implements IResponse
{
	protected $redirectUrl = null;
	protected $status = null;

	public function __construct($redirectUrl, $status = 302)
	{
		$this->status = $status == 301 ? 301 : 302;
		$this->redirectUrl = $redirectUrl;
	}

	public function Render()
	{
		if ($this->status = 301)
			header("HTTP/1.1 301 Moved Permanently");

		header("Location: {$this->redirectUrl}");
		exit;
	}
}