<?php

namespace Main\View;

use Helper\Storage;

class View implements IResponse
{
	protected $file = null;

	function __construct($templateFile, array $vars = null)
	{
		$this->file = $templateFile;

		if (is_array($vars))
			Buffer::getInstance()->setTemplateVar(null, $vars);
	}

	public function Render()
	{
		return Storage::getTwig()->render($this->file, Buffer::getInstance()->getTemplateVars());
	}
}