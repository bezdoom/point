<?php

namespace Main\View;

use Helper\Storage;

class Response implements IResponse
{
	protected $string = null;

	function __construct($string, array $vars = null)
	{
		$this->string = $string;

		if (is_array($vars))
			Buffer::getInstance()->setTemplateVar(null, $vars);
	}

	public function Render()
	{
		return Storage::getTwigString()->render($this->string, Buffer::getInstance()->getTemplateVars());
	}
}