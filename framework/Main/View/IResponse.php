<?php

namespace Main\View;

interface IResponse
{
	public function Render();
}