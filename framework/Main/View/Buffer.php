<?php

namespace Main\View;

use Helper\Storage;
use Main\Context\HttpApplication;
use Main\Context\HttpRequest;
use Main\Context\HttpResponse;
use Main\User\UserIdentify;

class Buffer
{
	private static $instance = null;

	public static function getInstance()
	{
		if (!self::$instance)
			self::$instance = new self();

		return self::$instance;
	}

	protected $envPath = null;
	protected $templateVars = array();

	protected function __clone() {}
	protected function __construct()
	{
		$this->setTemplateVar("application", HttpApplication::getInstance());
		$this->setTemplateVar("user", UserIdentify::getInstance());
        $this->setTemplateVar("uri", Storage::getCurrentUri());
        $this->setTemplateVar("request", HttpRequest::getInstance());
        $this->setTemplateVar("response", HttpResponse::getInstance());
	}

	public function setTemplateVar($key, $value)
	{
		if (!$key and is_array($value))
			$this->templateVars = $value;
		else
			$this->templateVars[$key] = $value;
	}

	public function unsetTemplateVar($key)
	{
		unset($this->templateVars[$key]);
	}

	/**
	 * @param $path
	 */
	public function registerEnvPath($path)
	{
		$this->envPath = $path;
	}

	/**
	 * @return string
	 */
	public function getEnvPath()
	{
		return $this->envPath;
	}

	/**
	 * @return array
	 */
	public function &getTemplateVars()
	{
		return $this->templateVars;
	}
}