<?php

namespace Main\Models;

use Main\DBLayer\baseLayer;

class Option extends baseLayer
{
	protected $tableName = "p_Options";

	protected $_fields = [
		"code" => [
			"TYPE" => "VARCHAR",
            "LENGTH" => 50,
			"KEY" => "PRIMARY",
            "TITLE" => "Символьный код"
		],
        "description" => [
            "TYPE" => "VARCHAR",
            "LENGTH" => 150,
            "TITLE" => "Описание опции"
        ],
        "value" => [
            "TYPE" => "VARCHAR",
            "LENGTH" => 255,
            "TITLE" => "Сохраненное значение"
        ]
	];
}