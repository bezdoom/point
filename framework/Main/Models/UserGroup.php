<?php

namespace Main\Models;

use Main\DBLayer\baseLayer;

class UserGroup extends baseLayer
{
	protected $tableName = "p_UserGroup";

	protected $_fields = [
		"id" => [
			"TYPE" => "INT",
			"AUTO_INCREMENT" => true,
			"KEY" => "PRIMARY",
            "TITLE" => "ID"
		],
		"active" => [
			"TYPE" => "ENUM('Y','N')",
			"DEFAULT" => "Y",
            "TITLE" => "Активность"
		],
		"code" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 20,
			"KEY" => "UNIQUE",
            "TITLE" => "Символьный код"
		],
		"adminAccess" => [
			"TYPE" => "ENUM('Y','N')",
			"DEFAULT" => "N",
            "TITLE" => "Админский доступ"
		],
		"description" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 255,
            "TITLE" => "Описание"
		],
	];

	/**
	 * @return bool
	 */
	public function isAdminAccess()
	{
		return $this->adminAccess == 'Y';
	}
}