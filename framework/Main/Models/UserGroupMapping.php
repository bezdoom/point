<?php

namespace Main\Models;

use Main\DBLayer\baseLayer;

class UserGroupMapping extends baseLayer
{
	protected $tableName = "p_UserGroupMapping";

	protected $_fields = [
		"id" => [
			"TYPE" => "INT",
			"AUTO_INCREMENT" => true,
			"KEY" => "PRIMARY"
		],
		"active" => [
			"TYPE" => "ENUM('Y','N')",
			"DEFAULT" => "Y"
		],
		"userId" => [
			"TYPE" => "INT",
			"KEY" => "INDEX"
		],
		"groupId" => [
			"TYPE" => "INT",
			"KEY" => "INDEX"
		],
	];
}