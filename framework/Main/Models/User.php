<?php

namespace Main\Models;

use Main\DBLayer\baseLayer;
use Main\DBLayer\Finder;

class User extends baseLayer
{
	protected $tableName = "p_User";

	/**
	 * @var UserGroup[]
	 */
	protected $groups = null;

	protected $_fields = [
		"id" => [
			"TYPE" => "INT",
			"AUTO_INCREMENT" => true,
			"KEY" => "PRIMARY",
			"TITLE" => "ID"
		],
		"active" => [
			"TYPE" => "ENUM('Y','N')",
			"DEFAULT" => "Y",
			"TITLE" => "Активность"
		],
		"email" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 255,
			"KEY" => "UNIQUE",
			"TITLE" => "E-Mail"
		],
		"salt" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 50,
			"TITLE" => "Примесь",
			"SKIP_ADMIN" => true
		],
		"password" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 50,
			"TITLE" => "Пароль",
			"SKIP_ADMIN" => true
		],
		"firstName" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 100,
			"TITLE" => "Имя"
		],
		"secondName" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 100,
			"TITLE" => "Отчество"
		],
		"lastName" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 100,
			"TITLE" => "Фамилия"
		],
		"sex" => [
			"TYPE" => "ENUM('male','female')'",
			"DEFAULT" => "NULL",
			"TITLE" => "Пол"
		],
        "created_at" => [
            "TYPE" => "DATETIME",
            "TITLE" => "Дата создания"
        ],
        "update_at" => [
            "TYPE" => "DATETIME",
            "TITLE" => "Дата обновления"
        ],
        "last_authorized_at" => [
            "TYPE" => "DATETIME",
            "TITLE" => "Последняя авторизация"
        ]
	];

	/**
	 * @return void
	 */
	protected function generateSalt()
	{
		$this->salt = md5(uniqid(rand(0,rand())));
	}

	/**
	 * @param string $newPassword
	 * @return void
	 */
	public function updatePassword($newPassword)
	{
		$this->generateSalt();
		$this->password = md5($this->salt . md5($newPassword));
	}

	/**
	 * @param string $password
	 *
	 * @return bool
	 */
	public function validatePassword($password)
	{
		return $this->password == md5($this->salt . md5($password));
	}

	/**
	 * @return UserGroup[]
	 */
	public function getUserGroupCollection()
	{
		if (is_array($this->groups))
			return $this->groups;

		/*
		 * todo: это нужно переписать с использованием орм связи
		 */
		$sql = "
			SELECT
			  `group`.`id` as `id`,
			  `group`.`active` as `active`,
			  `group`.`code` as `code`,
			  `group`.`adminAccess` as `adminAccess`,
			  `group`.`description` as `description`
			FROM `p_UserGroupMapping`  as `map`
			INNER JOIN `p_UserGroup` as `group` on `group`.`id` = `map`.`groupId`
			WHERE `map`.`active` = 'Y' AND `group`.`active` = 'Y'
		";

		$this->groups = Finder::Model(UserGroup::class)->find($sql)->getCollection();

		return $this->groups;
	}
}