<?php

namespace Main\HTML;

class Input extends Tag
{
	public $tagName = "input";
	public $hasClose = false;

	function __construct($name, $value = "", $type = "text")
	{
		$this->setName($name);
		$this->setType($type);
		$this->setValue($value);
	}

	public function setType($type)
	{
		return $this->setProperty("type", $type);
	}

	public function setValue($value)
	{
		return $this->setProperty("value", $value);
	}
}