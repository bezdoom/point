<?php

namespace Main\HTML;

use Helper\Storage;

class Form extends Tag
{
	public $tagName = "form";
	public $hasClose = true;

	function __construct($action = null, $method = "get", $encode = null)
	{
        if (is_null($method))
            $method = "get";

        $this->setMethod($method);

        if (is_null($action))
            $action = Storage::getCurrentUri()->getRequestPath();

		$this->setAction($action);

        if (!is_null($encode))
		    $this->setEncode($encode);
	}

	public function setAction($type)
	{
		return $this->setProperty("action", $type);
	}

	public function setMethod($value)
	{
		return $this->setProperty("method", $value);
	}

    public function setEncode($encode)
    {
        return $this->setProperty("encode", $encode);
    }
}