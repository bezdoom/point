<?php

namespace Main\HTML;

use Helper\String;

class Tag
{
	public $tagName = null;
	public $hasClose = null;
	protected $content = String::STRING_EMPTY;
	protected $property = array();
	protected $xhtmlEnd = false;

	function __construct($tagName, $hasClose = false)
	{
		$this->tagName = $tagName;
		$this->hasClose = $hasClose;
	}

	protected function getPropertyString()
	{
		$tmp = array();
		foreach($this->property as $key => $value)
			$tmp[] = "{$key}=\"" . htmlspecialchars($value) . "\"";

		if (empty($tmp))
			return String::STRING_EMPTY;

		return " " . implode(" ", $tmp);
	}

	protected function getEndXhtml()
	{
        if ($this->hasClose)
            return "";

		return $this->xhtmlEnd ? " /" : "";
	}

    protected function getContent()
    {
        return $this->content;
    }

	public function addContent($content)
	{
		if (!$this->hasClose)
			trigger_error("Content allow only for closed-tag");

		$this->content = $content;
		return $this;
	}

    public function mergeProperties(array $properties)
    {
        $this->property = array_merge($this->property, $properties);
        return $this;
    }

	public function setProperties(array $properties)
	{
		$this->property = $properties;
		return $this;
	}

	public function setProperty($key, $value)
	{
		if (!$value)
			return $this;

		$this->property[$key] = $value;
		return $this;
	}

	public function setXhtmlEnd($xhtml = true)
	{
		if (!is_bool($xhtml))
			$xhtml = true;

		$this->xhtmlEnd = (bool) $xhtml;
	}

	public function setId($id)
	{
		return $this->setProperty("id", $id);
	}

	public function setStyle($style)
	{
		return $this->setProperty("style", $style);
	}

	public function setClass($class)
	{
		return $this->setProperty("class", $class);
	}

	public function setName($name)
	{
		return $this->setProperty("name", $name);
	}

    public function getOpenHTML()
    {
        return "<{$this->tagName}{$this->getPropertyString()}{$this->getEndXhtml()}>";
    }

    public function getCloseHTML()
    {
        if (!$this->hasClose)
            return "";

        return "</{$this->tagName}>";
    }

	public function getHTML()
	{
		if ($this->hasClose)
			return $this->getOpenHTML() . $this->getContent() . $this->getCloseHTML();

		return $this->getOpenHTML();
	}

	public function __toString()
	{
		return $this->getHTML();
	}
}