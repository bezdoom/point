<?php

namespace Main\Routing;

class DefaultRoute extends Route
{
	public function __construct() {}

	public function getArguments()
	{
		return [
			"action" => null,
			"controller" => $this->routeMap->getDefaultControllerName()
		];
	}
}