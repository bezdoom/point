<?php

namespace Main\Routing;

class NotFoundRoute extends Route
{
	public function __construct() {}

	public function getArguments()
	{
		return [
			"action" => null,
			"controller" => $this->routeMap->getNotFoundControllerName()
		];
	}
}