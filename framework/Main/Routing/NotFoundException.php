<?php

namespace Main\Routing;

use Kernel\System\PointRuntimeException;

class NotFoundException extends PointRuntimeException {}