<?php

namespace Main\Routing;

use Helper\Storage;

class RouteMap
{
	protected $defaultController = null;
	protected $notFoundController = null;

	/**
	 * @var self
	 */
	private static $instance = null;

	/**
	 * @return self
	 */
	public static function &getInstance()
	{
		if (!self::$instance)
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * @var Route[]
	 */
	protected $map = array();
	/**
	 * @var Route
	 */
	protected $currentRoute = null;
	protected $hasDetection = false;
	protected $controllerNamespace = null;

	protected function __clone() {}
	protected function __construct() {}

	protected function determineRoute()
	{
		$this->hasDetection = true;
		foreach($this->map as &$route)
		{
			if ($route->isCurrent())
			{
				$this->currentRoute = $route;
				break;
			}
		}
	}

	public function registerDefaultControllerName($controllerName)
	{
		$this->defaultController = $controllerName;
		return $this;
	}

	public function registerNotFoundControllerName($controllerName)
	{
		$this->notFoundController = $controllerName;
		return $this;
	}

	public function registerRoute(Route &$route)
	{
		$route->setRouteMap($this);
		$this->map[] = $route;
	}

	public function registerControllerNamespace($namespace)
	{
		$this->controllerNamespace = $namespace;
	}

	protected function initDefaultRoute()
	{
		$this->hasDetection = true;
		$this->currentRoute = (new DefaultRoute())->setRouteMap($this);
	}

	protected function initNotFoundRoute()
	{
		$this->hasDetection = true;
		$this->currentRoute = (new NotFoundRoute())->setRouteMap($this);
	}

	public function getDefaultControllerName()
	{
		return $this->defaultController;
	}

	public function getNotFoundControllerName()
	{
		return $this->notFoundController;
	}

	public function getDefaultControllerClass()
	{
		return $this->getControllerClass($this->getDefaultControllerName());
	}

	public function getNotFoundControllerClass()
	{
		return $this->getControllerClass($this->getNotFoundControllerName());
	}

	public function getControllerClass($controllerName = null)
	{
		if (!$controllerName)
			$controllerName = $this->getCurrentRoute()->getArguments()["controller"];

		return  $this->controllerNamespace . "\\" . $controllerName;
	}

	/**
	 * @return Route
	 */
	public function &getCurrentRoute()
	{
		if ($this->currentRoute)
			return $this->currentRoute;

		if (Storage::getCurrentUri()->isRootDirectory())
			$this->initDefaultRoute();
		else
			$this->determineRoute();

		if (!$this->currentRoute)
			$this->initNotFoundRoute();

		return $this->currentRoute;
	}
}