<?php

namespace Main\Routing;

use Helper\Storage;

class Route
{
	/**
	 * @var string
	 */
	protected $urlPattern = null;
	/**
	 * @var array
	 */
	protected $options = null;
	/**
	 * @var RouteMap
	 */
	protected $routeMap = null;
	/**
	 * @var array
	 */
	protected $vars = array();
	protected $arguments = null;

	protected $controller = null;

	function __construct($urlPattern, array $options)
	{
		$this->urlPattern = $urlPattern;
		$this->options = $options;
	}

	/**
	 * @param RouteMap $routeMap
	 *
	 * @return $this
	 */
	public function setRouteMap(RouteMap &$routeMap)
	{
		$this->routeMap = $routeMap;
		return $this;
	}

	/**
	 * @return array
	 */
	public function &getArguments()
	{
		if (is_array($this->arguments))
			return $this->arguments;

		$this->arguments = array();
		foreach($this->options as $name => $value)
		{
			$key = preg_replace('![^0-9]!', '', $value);

			$this->arguments[$name] = $value;
			if (array_key_exists($key, $this->vars))
				$this->arguments[$name] = $this->vars[$key][0];
		}

		return $this->arguments;
	}

	/**
	 * @return bool
	 */
	public function isCurrent()
	{
		$pregCheck = (preg_match_all($this->urlPattern, Storage::getCurrentUri()->getRequestPath(), $this->vars) > 0);

        if (!isset($this->vars[0][0]))
            return false;

        return $pregCheck and ($this->vars[0][0] == Storage::getCurrentUri()->getRequestPath());
	}

}