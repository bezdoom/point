<?php

namespace Main\User\AuthProvider;

/**
 * Class BitrixMigration
 * @package Main\User\AuthProvider
 *
 * Провайдер авторизации, осуществляет проверку пользователя в новой и старой базах
 * и мигрирует его в текущую, для осуществления авторизации старыми пользователями.
 *
 * Реализует схему "Миграция пользователей.vsd"
 */
class BitrixMigration extends Direct
{
    /**
     * @return bool
     */
    public function Authorize()
    {
        $user = $this->getUser();

        if (!$user)
            return false;

        // todo
    }
}