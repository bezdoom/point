<?php

namespace Main\User\AuthProvider;

use Main\DBLayer\Finder;
use Main\DBLayer\Logic\FieldMatcher;
use Main\DBLayer\Logic\GroupAnd;
use Main\Models\User;

class Direct extends AuthProvider
{
	protected $email = null;
	protected $password = null;

	function __construct($email, $password)
	{
		$this->email = $email;
		$this->password = $password;
	}

    /**
     * @return \Main\DBLayer\Logic\AbstractGroup
     */
    protected function getUserFilter()
    {
        return new GroupAnd(
            new FieldMatcher("active", FieldMatcher::MODE_EQUAL, "Y"),
            new FieldMatcher("email", FieldMatcher::MODE_EQUAL, $this->email)
        );
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        return Finder::Model(User::class)->where($this->getUserFilter())->limit(1)->find()->current();
    }

	/**
	 * @return bool
	 */
	public function Authorize()
	{
		$user = $this->getUser();

		if (!$user)
			return false;

		if (!$user->validatePassword($this->password))
			return false;

		$this->userIdentify->setCurrentUser($user);
		return true;
	}
}