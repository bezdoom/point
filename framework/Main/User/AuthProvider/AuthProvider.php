<?php

namespace Main\User\AuthProvider;

use Main\User\UserIdentify;

abstract class AuthProvider
{
	/**
	 * @var UserIdentify
	 */
	protected $userIdentify = null;

	public function setUserIdentify(UserIdentify &$userIdentify)
	{
		$this->userIdentify = $userIdentify;
	}

	/**
	 * @return bool
	 */
	abstract public function Authorize();
}