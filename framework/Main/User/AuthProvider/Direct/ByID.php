<?php

namespace Main\User\AuthProvider\Direct;

use Main\DBLayer\Logic\FieldMatcher;
use Main\DBLayer\Logic\GroupAnd;
use Main\User\AuthProvider\Direct;

class ByID extends Direct
{
	protected $id = null;

	function __construct($id)
	{
		$this->id = $id;
	}

    /**
     * @return \Main\DBLayer\Logic\AbstractGroup
     */
    protected function getUserFilter()
    {
        return new GroupAnd(
            new FieldMatcher("active", FieldMatcher::MODE_EQUAL, "Y"),
            new FieldMatcher("id", FieldMatcher::MODE_EQUAL, $this->id)
        );
    }
}