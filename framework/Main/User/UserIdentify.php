<?php

namespace Main\User;

use Main\DBLayer\DBLayerException;
use Main\Models\User;
use Main\User\AuthProvider\AuthProvider;

class UserIdentify
{
	const SESSION_AUTH_ROOT = "_auth";
	const SESSION_AUTH_KEY = "userId";

	private static $instance = null;

	public static function getInstance()
	{
		if (!self::$instance)
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * @var User
	 */
	protected $current = null;
	protected $adminAccess = null;

	protected function __clone() {}
	protected function __construct()
	{
		$this->authFromSession();
	}

	/**
	 * @return void
	 */
	protected function rememberSession()
	{
		if ($this->isAuthorized())
			$_SESSION[POINT_ROOT_SESSION][self::SESSION_AUTH_ROOT][self::SESSION_AUTH_KEY] = $this->getCurrent()->getPrimaryValue();
	}

	/**
	 * @return bool
	 */
	protected function authFromSession()
	{
		if (!isset($_SESSION[POINT_ROOT_SESSION][self::SESSION_AUTH_ROOT][self::SESSION_AUTH_KEY]))
			return false;

		$userPK = intval($_SESSION[POINT_ROOT_SESSION][self::SESSION_AUTH_ROOT][self::SESSION_AUTH_KEY]);

		if ($userPK <= 0)
			return false;

		try
		{
			$this->setCurrentUser(new User($userPK));
		}
		catch (DBLayerException $e)
		{
			return false;
		}
		finally
		{
			return true;
		}
	}

    protected function updateAuthorizedDate()
    {
        if (!$this->current)
            return false;

        if (!$this->isAuthorized())
            return false;

        $this->current->last_authorized_at = (new \DateTime())->format("Y-m-d H:i:s");
        return $this->current->Save() > 0;
    }

	/**
	 * @return bool
	 */
	public function isAuthorized()
	{
		return $this->current !== null;
	}

	/**
	 * @param AuthProvider $provider
	 *
	 * @return bool
	 */
	public function Authorize(AuthProvider &$provider)
	{
		if ($this->isAuthorized())
			return true;

		$provider->setUserIdentify($this);
		if ( $provider->Authorize() )
		{
			$this->rememberSession();
            $this->updateAuthorizedDate();
			return true;
		}

		return false;
	}

    /**
     * @param User $user
     * @return bool
     */
    public function Register(User $user)
    {
        if (!$user)
            return false;


    }

	public function Logout()
	{
		unset($_SESSION[POINT_ROOT_SESSION][self::SESSION_AUTH_ROOT]);
	}

	/**
	 * @param User $user
	 */
	public function setCurrentUser(User $user)
	{
		$this->current = $user;
	}

	/**
	 * @return int
	 */
	public function getUserID()
	{
		if (!$this->getCurrent())
			return 0;

		return (int) $this->getCurrent()->getPrimaryValue();
	}

	/**
	 * @return \Main\Models\UserGroup[]
	 */
	public function getGroups()
	{
		if (!$this->getCurrent())
			return array();

		return $this->getCurrent()->getUserGroupCollection();
	}

	/**
	 * @param $groupCode
	 *
	 * @return bool
	 */
	public function inGroup($groupCode)
	{
		if (!$this->getCurrent())
			return false;

		foreach($this->getCurrent()->getUserGroupCollection() as $group)
		{
			if ($group->code != $groupCode)
				continue;

			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function isAdmin()
	{
		if (!$this->getCurrent())
			return false;

		if (is_null($this->adminAccess))
		{
			$this->adminAccess = false;

			foreach ( $this->getCurrent()->getUserGroupCollection() as $group )
			{
				if (!$group->isAdminAccess())
					continue;

				$this->adminAccess = true;
				break;
			}
		}

		return $this->adminAccess;
	}

	/**
	 * @return \Main\Models\User
	 */
	public function getCurrent()
	{
		return $this->current;
	}
}