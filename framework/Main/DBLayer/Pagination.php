<?php

namespace Main\DBLayer;

use Helper\Storage;

class Pagination
{
	const DEFAULT_TEMPLATE = "_system/components/pagination.twig";

	/**
	 * @var ResultCollection
	 */
	protected $collection = null;

	protected $pageSize = null;
	protected $totalCount = null;
	protected $currentPage = null;

	protected $pagesCount = null;
	protected $previousPage = null;
	protected $nextPage = null;

	protected $getKey = null;

	public function __construct(ResultCollection &$collection)
	{
		$this->collection = $collection;

		$this->getKey = $this->collection->getFinder()->getGetKey();
		$this->pageSize = $this->collection->getFinder()->getPageSize();
		$this->totalCount = $this->collection->getFinder()->getTotalCount();
		$this->currentPage = $this->collection->getFinder()->getCurrentPage();
	}

	/**
	 * @return string
	 */
	public function getGetKey()
	{
		return $this->getKey;
	}

	/**
	 * @return int
	 */
	public function getCurrentPage()
	{
		return $this->currentPage;
	}

	/**
	 * @return int
	 */
	public function getTotalCount()
	{
		return $this->totalCount;
	}

	/**
	 * @return null
	 */
	public function getPagesCount()
	{
		if (!$this->pagesCount)
		{
			$this->pagesCount = 1;
			if ($this->pageSize > 0)
				$this->pagesCount = ceil($this->totalCount / $this->pageSize);
		}

		return $this->pagesCount;
	}

	/**
	 * @return int
	 */
	public function getPreviousPage()
	{
		if ($this->currentPage <= 1)
			return 1;

		return (int) ($this->currentPage - 1);
	}

	/**
	 * @return int
	 */
	public function getNextPage()
	{
		if ($this->currentPage >= $this->getPagesCount())
			return $this->getPagesCount();

		return (int) ($this->currentPage + 1);
	}

	/**
	 * @param int $margin
	 *
	 * @return array
	 */
	public function getPagesStack($margin = null)
	{
		if (!is_numeric($margin) or $this->getPagesCount() <= $margin * 2)
			return range(1, $this->getPagesCount());

		$leftMargin = $this->getCurrentPage() - $margin;
		$rightMargin = $this->getCurrentPage() + $margin;

		if ($leftMargin < 1)
		{
			$rightMargin += (-1 - ($leftMargin - 2));
			$leftMargin = 1;
		}

		if ($rightMargin > $this->getPagesCount())
		{
			$leftMargin -= ($rightMargin - $this->getPagesCount());
			$rightMargin -= ($rightMargin - $this->getPagesCount());
		}

		return range($leftMargin, $rightMargin);
	}

	/**
	 * @param $template
	 *
	 * @return string
	 */
	public function Render($template = self::DEFAULT_TEMPLATE)
	{
		$twig = Storage::getTwig();
		return $twig->render($template, array("this" => $this));
	}
}