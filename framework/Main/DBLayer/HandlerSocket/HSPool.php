<?php

namespace Main\DBLayer\HandlerSocket;

use HSPHP\ReadSocket;
use HSPHP\WriteSocket;

class HSPool
{
    /**
     * @var self
     */
    private static $instance = null;

    private static $host = null;
    private static $readPort = null;
    private static $writePort = null;

    /**
     * @var ReadSocket
     */
    protected $socket = null;
    /**
     * @var WriteSocket
     */
    protected $writeSocket = null;

    public static function setConnectionSettings($host = "localhost", $writePort = 9999, $readPort = 9998)
    {
        self::$host = $host;
        self::$readPort = $readPort;
        self::$writePort = $writePort;
    }

    /**
     * @static
     * @return self
     */
    public static function getConnection()
    {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }

    /**
     * @return ReadSocket
     */
    public function getSocket()
    {
        if (is_null($this->socket))
        {
            $this->socket = new ReadSocket();
            $this->socket->connect(self::$host, self::$readPort);
        }

        return $this->socket;
    }

    /**
     * @return WriteSocket
     */
    public function getWriteSocket()
    {
        if (is_null($this->writeSocket))
        {
            $this->writeSocket = new WriteSocket();
            $this->writeSocket->connect(self::$host, self::$writePort);
        }

        return $this->writeSocket;
    }
}