<?php

namespace Main\DBLayer\HandlerSocket;

use HSPHP\ReadSocket;
use Main\DBLayer\baseLayer;
use Main\DBLayer\DBLayerException;

class HSFinder
{
    const MAX_LIMIT = 9999999999;

    protected static $workBase = null;

    /**
     * @var baseLayer
     */
    protected $model = null;
    protected $limit = null;
    protected $offset = null;
    protected $key = null;
    protected $condition = null;
    protected $value = array();

    /**
     * @param string $base
     */
    public static function setBase($base)
    {
        self::$workBase = $base;
    }

    /**
     * @return string
     */
    public static function getBase()
    {
        return self::$workBase;
    }

    public function __construct($model)
    {
        $this->model = new $model;

        if (!($this->model instanceof baseLayer))
            throw new DBLayerException("Bad model");

        $this->where();
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return $this
     */
    public function limit($limit = self::MAX_LIMIT, $offset = 0)
    {
        $this->limit = (int) $limit;
        $this->offset = (int) $offset;
        return $this;
    }

    /**
     * @param string $condition
     * @param array $value
     * @param string $key
     * @return $this
     */
    public function where($condition = ">", array $value = array(0), $key = "")
    {
        $this->condition = $condition;
        $this->value = $value;
        $this->key = $key;
        return $this;
    }

    /**
     * @return int
     */
    protected function getLimit()
    {
        if (is_null($this->limit))
            return self::MAX_LIMIT;

        if ((int) $this->limit < 0)
            return self::MAX_LIMIT;

        return (int) $this->limit;
    }

    /**
     * @return int
     */
    protected function getOffset()
    {
        if (is_null($this->offset))
            return 0;

        if ((int) $this->offset < 0)
            return 0;

        return (int) $this->offset;
    }

    /**
     * @param ReadSocket $hsocket
     * @return baseLayer[]
     */
    protected function getResultArray(ReadSocket $hsocket)
    {
        $result = array();
        $keys = $this->model->getFieldsList();

        foreach ($hsocket->readResponse() as $values)
            $result[] = new $this->model(array_combine($keys, $values));

        return $result;
    }

    /**
     * @return string
     */
    protected function getKey()
    {
        if (is_null($this->key))
            return "";

        return $this->key;
    }

    /**
     * @return string
     */
    protected function getFields()
    {
        return implode(",", $this->model->getFieldsList());
    }

    /**
     * @return string
     */
    protected function getCondition()
    {
        if (is_null($this->condition))
            return ">";

        return $this->condition;
    }

    /**
     * @return array
     */
    protected function getValue()
    {
        if (!is_array($this->value))
            return array(0);

        if (count($this->value) > 1)
            return array();

        return $this->value;
    }

    /**
     * @return array
     */
    protected function getInValue()
    {
        if (!is_array($this->value))
            return array();

        if (count($this->value) > 1)
            return $this->value;

        return array();
    }

    /**
     * @return \Main\DBLayer\baseLayer[]
     */
    public function find()
    {
        $hsocket = HSPool::getConnection()->getSocket();

        $id = $hsocket->getIndexId($this->getBase(), $this->model->getTable(), $this->getKey(), $this->getFields());
        $hsocket->select($id, $this->getCondition(), $this->getValue(), $this->getLimit(), $this->getOffset(), $this->getInValue());

        return $this->getResultArray($hsocket);
    }

    /**
     * @param $model
     * @return self
     */
    public static function Model($model)
    {
        return new self($model);
    }
}