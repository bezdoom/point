<?php

namespace Main\DBLayer;

use Main\DBLayer\Driver\PDO;
use Main\DBLayer\Logic\LogicInterface;
use Main\Cache\Manager as CacheManager;

class Finder
{
    const PREPARE_ACTION_REGEXP_PATTERN = '#(SELECT|FROM|JOIN)([\n ]+)(?:[`]*){=([A-z]+)}(?:[`]*)#is';

	/**
	 * @var string
	 */
	protected $modelClassName = null;
	/**
	 * @var int
	 */
	protected $cacheTTL = null;
	/**
	 * @var int
	 */
	protected $pageSize = null;
	/**
	 * @var int
	 */
	protected $currentPage = null;
	/**
	 * @var string
	 */
	protected $getKey = null;
	/**
	 * @var int
	 */
	protected $limit = null;

	/**
	 * @var baseLayer
	 */
	protected $metaClass = null;

	/**
	 * @var array
	 */
	protected $where = array();
	/**
	 * @var string
	 */
	protected $whereDefaultLogic = "AND";
	/**
	 * @var LogicInterface
	 */
	protected $logic = null;
	/**
	 * @var array
	 */
	protected $customSelect = array();
	/**
	 * @var array
	 */
	protected $ordering = array();

	function __construct($model)
	{
		$this->modelClassName = $model;
	}

	public static function Model($model)
	{
		return new self($model);
	}

	/**
	 * @return string
	 */
	protected function getFinderHash()
	{
		ob_start();
		print_r($this);
		return md5(ob_get_clean());
	}

	/**
	 * @return int
	 */
	public function getCurrentPage()
	{
		if (!$this->currentPage)
			$this->currentPage = isset($_GET[$this->getKey]) ? (int) $_GET[$this->getKey] : 1;

		return $this->currentPage;
	}

	/**
	 * @param int $ttl
	 *
	 * @return $this
	 */
	public function withCache($ttl = 3600)
	{
		$this->cacheTTL = null;

		if (intval($ttl))
			$this->cacheTTL = (int) $ttl;

		return $this;
	}

	/**
	 * @param int    $pageSize
	 * @param string $getKey
	 *
	 * @return $this
	 */
	public function inPages($pageSize, $getKey = "page")
	{
		$this->pageSize = null;
		$this->getKey = $getKey;

		if (intval($pageSize))
			$this->pageSize = (int) $pageSize;

		$this->getCurrentPage();

		return $this;
	}

	/**
	 * @param int $limit
	 *
	 * @return $this
	 */
	public function limit($limit)
	{
		$this->limit = null;

		if (intval($limit))
			$this->limit = (int) $limit;

		return $this;
	}

	/**
	 * @arguments string[]
	 *
	 * @return $this
	 */
	public function select()
	{
		$this->customSelect = array();
		$arg = func_get_args();
		if (!empty($arg))
			$this->customSelect = $arg;

		return $this;
	}

	/**
	 * @param LogicInterface $logic
	 *
	 * @return $this
	 */
	public function where(LogicInterface $logic)
	{
		$this->logic = $logic;

		return $this;
	}

	/**
	 * @param array  $where
	 * @param string $defaultLogic
	 *
	 * @return $this
	 */
	public function whereArray(array $where, $defaultLogic = "AND")
	{
		$this->where = $where;
		$this->whereDefaultLogic = $defaultLogic;

		return $this;
	}

	/**
	 * @param array $ordering
	 *
	 * @return $this
	 */
	public function order(array $ordering)
	{
		$this->ordering = $ordering;

		return $this;
	}

	/**
	 * @return baseLayer
	 * @throws DBLayerException
	 */
	protected function getMetaClass()
	{
		if ($this->metaClass)
			return $this->metaClass;

		if ( !class_exists( $this->modelClassName ) )
			throw new DBLayerException( "Can not find class: {$this->modelClassName}" );

		$metaClass = new $this->modelClassName();
		if ( !( $metaClass instanceof baseLayer ) )
			throw new DBLayerException( "Class {$this->modelClassName} does not instance of baseLayer" );

		$this->metaClass = $metaClass;

		return $this->metaClass;
	}

	/**
	 * @return bool
	 */
	protected function needCache()
	{
		return $this->getCacheTTL() > 0;
	}

	/**
	 * @return int
	 */
	protected function getCacheTTL()
	{
		if (!$this->cacheTTL)
			return 0;

		return intval($this->cacheTTL) ? (int) $this->cacheTTL : 0;
	}

	/**
	 * @return array
	 */
	public function getModelMeta()
	{
		return $this->getMetaClass()->getMetaModel();
	}

	/**
	 * @return int
	 */
	public function getPageSize()
	{
		if ($this->pageSize > 0)
			return $this->pageSize;

		return 0;
	}

	/**
	 * @return string
	 */
	public function getGetKey()
	{
		return $this->getKey;
	}

	/**
	 * @return bool
	 */
	public function needPaging()
	{
		return ($this->getPageSize() > 0);
	}

	/**
	 * @return bool
	 */
	protected function needLimit()
	{
		return ($this->limit && (int) $this->limit > 0);
	}

	/**
	 * @return bool
	 */
	protected function needCustomSelect()
	{
		return !empty($this->customSelect);
	}

	/**
	 * @return array
	 */
	protected function getSelectFields()
	{
		if ($this->needCustomSelect())
			return $this->buildCustomSelectList();

		return $this->buildFullSelectList();
	}

	/**
	 * @return array
	 */
	protected function buildFullSelectList()
	{
		$select = array();

		foreach($this->getMetaClass()->getFieldsList() as $field)
			$select[] = "\n\t`" . $field . "`";

		return $select;
	}

	/**
	 * @return array
	 */
	protected function buildCustomSelectList()
	{
		$select = array();

		foreach($this->customSelect as $field)
			$select[] = "\n\t" . "`{$field}`";

		return $select;
	}

	/**
	 * @return string
	 */
	protected function getFieldsSqlPartition()
	{
		return implode(", ", $this->getSelectFields());
	}

	/**
	 * @return string
	 */
	protected function getTableSqlPartition()
	{
		return "\n" . " FROM `".$this->getMetaClass()->getTable()."`";
	}

	/**
	 * @return string
	 */
	protected function getWhereSqlPartition()
	{
		if (empty($this->where) && !$this->logic)
			return "";

		if ($this->logic)
			return "\n" . " WHERE " . $this->logic->getString();

		return "\n" . " WHERE " . implode(" {$this->whereDefaultLogic} ", $this->where);
	}

	/**
	 * @return string
	 */
	protected function getOrderingSqlPartition()
	{
		if (empty($this->ordering))
			return "";

		return "\n" . " ORDER BY " . implode(", ", $this->ordering);
	}

	/**
	 * @return string
	 */
	protected function getLimitSqlPartition()
	{
		if ($this->needPaging())
		{
			if ($this->currentPage < 1)
				$this->currentPage = 1;

			$offset = ($this->currentPage * $this->pageSize) - $this->pageSize;

			return "\r" . " LIMIT {$offset},{$this->pageSize}";
		}

		if ($this->needLimit())
			return "\r" . " LIMIT {$this->limit}";

		return "";
	}

	/**
	 * @return string
	 */
	protected function buildSelectSql()
	{
		return $sql = "SELECT "
			. $this->getFieldsSqlPartition()
			. $this->getTableSqlPartition()
			. $this->getWhereSqlPartition()
			. $this->getOrderingSqlPartition()
			. $this->getLimitSqlPartition();
	}

	/**
	 * @return string
	 */
	protected function buildCountAllSql()
	{
		return $sql = "SELECT COUNT(*) as `count` "
			. $this->getTableSqlPartition()
			. $this->getWhereSqlPartition();
	}

	protected function getCountAllSql()
	{
		if (!defined("CACHE_TTL_FINDER_SQL") or CACHE_TTL_FINDER_SQL <= 0)
			return $this->buildCountAllSql();

		$cacheKey = $this->getMetaClass()->getTable() . "_countall_" . $this->getFinderHash();
		if (false === $sql = CacheManager::getInstance()->Load($cacheKey, CACHE_TTL_FINDER_SQL))
		{
			$sql = $this->buildCountAllSql();

			if (CACHE_TTL_FINDER_SQL > 0)
				CacheManager::getInstance()->Save($cacheKey, $sql);
		}

		return $sql;
	}

	/**
	 * @return string
	 */
	protected function getSelectSql()
	{
		if (!defined("CACHE_TTL_FINDER_SQL") or CACHE_TTL_FINDER_SQL <= 0)
			return $this->buildSelectSql();

		$cacheKey = $this->getMetaClass()->getTable() . "_" . $this->getFinderHash();
		if (false === $sql = CacheManager::getInstance()->Load($cacheKey, CACHE_TTL_FINDER_SQL))
		{
			$sql = $this->buildSelectSql();

			if (CACHE_TTL_FINDER_SQL > 0)
				CacheManager::getInstance()->Save($cacheKey, $sql);
		}

		return $sql;
	}

	/**
	 * @param     $sql
	 * @param int $fetch_style
	 *
	 * @return array
	 */
	protected function fetchAll($sql, $fetch_style = PDO::FETCH_ASSOC)
	{
		return ConnectionPool::getConnection()->getPdo()->query( $sql )->fetchAll( $fetch_style );
	}

	/**
	 * @param     $sql
	 * @param int $fetch_style
	 *
	 * @return array
	 */
	protected function fetchAllWithCache($sql, $fetch_style = PDO::FETCH_ASSOC)
	{
		$cacheKey = "sql_orm_" . md5($sql);

		if (false === $selectedArray = CacheManager::getInstance()->Load($cacheKey, $this->getCacheTTL()))
		{
			$selectedArray = $this->fetchAll($sql, $fetch_style);

			// Кэшируем запрос, если время жизни кеша больше 0
			if ($this->needCache())
				CacheManager::getInstance()->Save($cacheKey, $selectedArray);
		}

		return $selectedArray;
	}

    /**
     * @param array $matches
     * @return string
     */
    protected function doPrepareAction(array $matches)
    {
        $method = array($this, "prepareAction_" . $matches[3]);
        if (!is_callable($method))
            return "";

        return $matches[1] . $matches[2]. call_user_func($method);
    }

    /**
     * @return string
     */
    protected function prepareAction_table()
    {
        return $this->getMetaClass()->getTable();
    }

    /**
     * @return string
     */
    protected function prepareAction_fields()
    {
        return $this->getFieldsSqlPartition();
    }

    /**
     * @param $sql
     * @return mixed
     */
    protected function prepareCustomSql($sql)
    {
        return preg_replace_callback(self::PREPARE_ACTION_REGEXP_PATTERN, array($this, "doPrepareAction"), $sql);
    }

	/**
	 * @return int
	 */
	public function getTotalCount()
	{
		$sql = $this->getCountAllSql();

		if (!$this->needCache())
			$selectedArray = $this->fetchAll($sql);
		else
			$selectedArray = $this->fetchAllWithCache($sql);

		return (int) $selectedArray[0]['count'];
	}

	/**
	 * @param string $sql
	 *
	 * @return ResultCollection
	 */
	public function find($sql = null)
	{
        if (is_string($sql))
            $sql = $this->prepareCustomSql($sql);

		if (!$sql)
			$sql = $this->getSelectSql();

		if (!$this->needCache())
			$selectedArray = $this->fetchAll($sql);
		else
			$selectedArray = $this->fetchAllWithCache($sql);

		// Собираем коллекцию моделей
		$result = array();
		$metaClass = $this->getMetaClass();
		foreach ($selectedArray as $element)
			$result[] = new $metaClass($element);

		return new ResultCollection($result, $this);
	}
}