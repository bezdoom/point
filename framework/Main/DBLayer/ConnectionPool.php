<?php

namespace Main\DBLayer;

use Main\DBLayer\Driver\PDO;
use Main\DBLayer\HandlerSocket\HSFinder;

class ConnectionPool
{
	/**
	 * @var self
	 */
	private static $instance = null;

	private static $dsn = null;
	private static $username = null;
	private static $passwd = null;
    private static $options = null;

	/**
	 * @static
	 * @return self
	 */
	public static function &getConnection()
	{
		if (!self::$instance)
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * @var PDO
	 */
	protected $pdo = null;

	private function __clone() {}
	private function __construct() {}

	/**
	 * @static
	 * @param string $user
	 * @param string $passwd
     * @param string $charset
	 * @param string $dbName
	 * @param string $host
	 * @param string $driver
     * @param array $options
	 */
	public static function setConnectionString($user, $passwd, $dbName, $host = "localhost", $charset = "utf8", $driver = "mysql", array $options = array())
	{
		self::$passwd = $passwd;
		self::$username = $user;
		self::$dsn = $driver . ":dbname=" . $dbName . ";host=" . $host . ";charset=" . $charset;
        self::$options = $options;

        HSFinder::setBase($dbName);
	}

	/**
	 * @throws \PDOException
	 *
	 * @return void
	 */
	protected function initPdo()
	{
		if (!self::$dsn or !is_array(self::$options))
			throw new \PDOException("Empty connection string");

		$this->pdo = new PDO(self::$dsn, self::$username, self::$passwd, self::$options);
	}

	/**
	 * @return PDO
	 */
	public function &getPdo()
	{
		if (!$this->pdo)
			$this->initPdo();

		return $this->pdo;
	}
	
}