<?php

namespace Main\DBLayer;

use Kernel\Dictionary;

class ResultCollection extends Dictionary
{
	/**
	 * @var Finder
	 */
	protected $finder = null;
	/**
	 * @var Pagination
	 */
	protected $pagination = null;

	public function __construct(array $collection, Finder &$finder)
	{
		$this->finder = $finder;
		$this->collection = $collection;
	}

	/**
	 * @return Finder
	 */
	public function &getFinder()
	{
		return $this->finder;
	}

	/**
	 * @return array
	 */
	public function getMetaModel()
	{
		return $this->finder->getModelMeta();
	}

	/**
	 * @return array
	 */
	public function getAdminMetaModel()
	{
		$meta = array();
		foreach($this->getMetaModel() as $key => $value)
		{
			if (isset($value["SKIP_ADMIN"]) and $value["SKIP_ADMIN"] === true)
				continue;

			$meta[$key] = $value;
		}

		return $meta;
	}

	/**
	 * @return Pagination
	 */
	public function &getPagination()
	{
		if (!$this->pagination)
			$this->pagination = new Pagination($this);

		return $this->pagination;
	}
}