<?php

namespace Main\DBLayer;

use Main\DBLayer\Driver\PDO;

abstract class baseLayer
{
	const MODE_TYPE_ORIGIN = 1;
	const MODE_TYPE_MODIFIED = 2;

	const HAS_MANY = "hasMany";

	/**
	 * @var string Тип таблицы
	 */
	protected $storageEngine = "InnoDB";
	/**
	 * @var string Кодировка таблицы по умолчанию
	 */
	protected $charset = "utf8";
	/**
	 * @var string Кодировка сравнения таблицы
	 */
	protected $collation = "utf8_general_ci";

	/**
	 * @var string Имя таблицы
	 */
	protected $tableName = null;
	/**
	 * @var array Описаная модель таблицы
	 */
	protected $_fields = null;

	/**
	 * @var string Первичный ключ (определяется автомтаически на основе описаной модели)
	 */
	private $primaryKey = null;

	/**
	 * @param null|mixed $model Значение первичного ключа или ассоциированный массив выборки
	 *
	 * @throws DBLayerException
	 *
	 * Создает новую модель. В случае указания $model знаечния уникального ключа модель сама достанет информацию о себе.
	 * Если передать в качестве $model ассоциированный массив то модель создастся без обращения к БД и будет заполнена указанными в массиве значениями.
	 */
	function __construct ( $model = null )
	{
		if ( is_null( $this->_fields ) || empty( $this->_fields ) )
			throw new DBLayerException( "You should describe the metadata model!" );

		$this->RefreshFromModel( $model );
	}

	/**
	 * @return Driver\PDO
	 */
	protected function &getConnection()
	{
		return ConnectionPool::getConnection()->getPdo();
	}

	/**
	 * @return int|null|string
	 * Возвращает первичный ключ
	 */
	public function getPrimaryKey ()
	{
		if ( is_null( $this->primaryKey ) ) {
			foreach ( $this->_fields as $key => $keyProperty )
				if ( isset( $keyProperty[ "KEY" ] ) && $keyProperty[ "KEY" ] == "PRIMARY" ) {
					$this->primaryKey = $key;
					break;
				}
		}

		return $this->primaryKey;
	}

	/**
	 * @return mixed
	 */
	public function getPrimaryValue ()
	{
		if (!$this->getPrimaryKey())
			return null;

		return $this->getValue($this->getPrimaryKey());
	}

	/**
	 * @param string $value новое значение для первичного ключа
	 *
	 * @throws DBLayerException
	 *
	 * Изменяет значение первичного ключа (по умолчанию данное поле ReadOnly)
	 */
	protected function setPrimaryKeyValue ( $value )
	{
		if ( !$this->getPrimaryKey() )
			throw new DBLayerException( "Can not verify the existence of a record without a declared primary key" );

		$this->_fields[ $this->getPrimaryKey() ][ "VALUE" ] = $value;
	}

    /**
     * @param string $name
     *
     * @return bool
     * @throws DBLayerException
     */
    protected function isAutoincrement( $name )
    {
        if ( !isset( $this->_fields[ $name ] ) )
            throw new DBLayerException( "Field {$name} does not exist" );

        return isset( $this->_fields[ $name ][ "AUTO_INCREMENT" ] ) && $this->_fields[ $name ][ "AUTO_INCREMENT" ] === true;
    }

	/**
	 * @param string $name Название поля
	 *
	 * @return bool
	 * @throws DBLayerException
	 *
	 * Проверяет доступность на запись поля $name
	 */
	protected function isReadOnly ( $name )
	{
		if ( !isset( $this->_fields[ $name ] ) )
			throw new DBLayerException( "Field {$name} does not exist" );

		return ( $this->isAutoincrement( $name )
			|| isset( $this->_fields[ $name ][ "READ_ONLY" ] ) && $this->_fields[ $name ][ "READ_ONLY" ] === true );
	}

	/**
	 * @return bool
	 * @throws DBLayerException
	 *
	 * Проверяет, существует ли запись с указанным первичным ключом в таблице
	 */
	protected function isExists ()
	{
		if ( !$this->getPrimaryKey() )
			throw new DBLayerException( "Can not verify the existence of a record without a declared primary key" );

		if ( !$this->getPrimaryValue() )
			return false;

		$query = "SELECT COUNT(`{$this->getPrimaryKey()}`)
				  FROM `{$this->tableName}`
				  WHERE `{$this->getPrimaryKey()}`='" . $this->getPrimaryValue() . "';";

		$res = $this->getConnection()->query( $query );
		return ( $res->fetchColumn() > 0 );
	}

	/**
	 * @return null|string
	 *
	 * Возвращает sql запрос на вставку нового элемента в таблицу
	 */
	protected function getInsertSQL ()
	{
		$column = array ();
		$values = array ();
		foreach ( $this->_fields as $fieldName => $fieldProperty ) {
			if ( $this->isAutoincrement( $fieldName ) )
				continue;

			$value = null;
			if ( isset( $fieldProperty[ "VALUE" ] ) )
				$value = $fieldProperty[ "VALUE" ];
			elseif ( isset( $fieldProperty[ "DEFAULT" ] ) )
				$value = $fieldProperty[ "DEFAULT" ];

			if ( is_null( $value ) )
				continue;

			$column[] = "`" . $fieldName . "`";
			$values[] = $this->getConnection()->quote($value);
		}

		if ( empty( $column ) && empty( $values ) )
			return null;

		$sql = "INSERT INTO `" . $this->tableName . "` (" . implode( ", ", $column ) . ") VALUES (" . implode( ", ", $values ) . ");";
		return $sql;
	}

	/**
	 * @return null|string
	 *
	 * Возвращает sql запрос на обновление элемента с указанным первичным ключом (только измененные поля)
	 */
	protected function getUpdateSQL ()
	{
		if ( !$this->getPrimaryValue() )
			return null;

		$updateItems = array();
		foreach($this->_fields as $fieldName => $fieldProperty)
		{
            if ( $this->isAutoincrement( $fieldName ) )
                continue;

			if (isset($fieldProperty["FIELD_CVS"]) && $fieldProperty["FIELD_CVS"] == self::MODE_TYPE_MODIFIED)
				if (isset($fieldProperty["VALUE"]))
					$updateItems[] = "`".$fieldName."`=".$this->getConnection()->quote($fieldProperty["VALUE"]);
		}

		if ( empty( $updateItems ) )
			return null;

		$sql = "UPDATE `" . $this->tableName . "` SET " . implode( ", ", $updateItems ) . " WHERE `{$this->getPrimaryKey()}`='{$this->getPrimaryValue()}';";
		return $sql;
	}

	/**
	 * @return null|string
	 *
	 * Возвращает sql запрос на выборку значений элемента с указанным первичным ключом
	 */
	protected function getSelectSQL ()
	{
		if ( !$this->getPrimaryValue() )
			return null;

		$select = array ();
		foreach ( $this->_fields as $fieldName => $fieldProperty )
			$select[ ] = "`" . $fieldName . "`";

		if ( empty( $select ) )
			return null;

		$sql = "SELECT " . implode( ", ", $select ) . " FROM `" . $this->tableName . "` WHERE `{$this->getPrimaryKey()}`='{$this->getPrimaryValue()}' LIMIT 1;";
		return $sql;
	}

	/**
	 * @return null|string
	 *
	 * Возвращает sql запрос на удаление записи с указанным первичным ключом
	 */
	protected function getDeleteSql ()
	{
		if ( !$this->getPrimaryValue() )
			return null;

		$sql = "DELETE FROM `" . $this->tableName . "` WHERE `{$this->getPrimaryKey()}`='{$this->getPrimaryValue()}' LIMIT 1;";
		return $sql;
	}

	/**
	 * @param array $fields Ассоциированный массив поле=>знаечние
	 *
	 * Обновляет все значения модели из входящего массива $fields
	 */
	protected function updateFieldsValues ( array $fields )
	{
		foreach ( $fields as $fieldColumn => $fieldValue ) {
			$this->_fields[ $fieldColumn ][ "VALUE" ] = $fieldValue;
			$this->_fields[ $fieldColumn ][ "FIELD_CVS" ] = self::MODE_TYPE_ORIGIN;
		}
	}

	/**
	 * @throws DBLayerException
	 *
	 * Достает значения полей по первичному ключу и обновляет значения для текущей модели
	 */
	protected function refreshFields ()
	{
		$sql = $this->getSelectSQL();

		if ( !$sql )
			throw new DBLayerException( "Empty query" );

		$res = $this->getConnection()->query( $sql );
		if ( $fields = $res->fetch( PDO::FETCH_ASSOC ) )
			$this->updateFieldsValues( $fields );
		else
			throw new DBLayerException( "Model with " . $this->getPrimaryKey() . " = " . $this->{$this->getPrimaryKey()} . " not found" );
	}

	/**
	 * @param sting $field Название поля
	 * @param mixed $value Новое значение поля
	 *
	 * Вносит изменения в значение текущей модели (без проверки на ReadOnly)
	 */
	public function writeFieldValue ( $field, $value )
	{
		$this->_fields[ $field ][ "VALUE" ] = $value;
		$this->_fields[ $field ][ "FIELD_CVS" ] = self::MODE_TYPE_MODIFIED;
	}

	/**
	 * @param int|string|array $model Значение первичного ключа или ассоцииррованный массив выборки
	 *
	 * Заполняет текущую модель знаечниями из ассоциированного массива,
	 * либо достает данные из БД по указанному значению первичного ключа.
	 */
	public function RefreshFromModel ( $model )
	{
		if ( !is_null( $model ) ) {
			if ( is_array( $model ) ) {
				$this->updateFieldsValues( $model );
			}
			else {
				$this->setPrimaryKeyValue( $model );
				$this->refreshFields();
			}
		}
	}

	/**
	 * @return int ID последней добавленной записи
	 *
	 * Сохраняет измнения модели.
	 * В случае, если это новая модель - создаст новую запись в БД.
	 * Если запись уже существует и указан первичный ключ - запись будет обновлена.
	 */
	public function Save ()
	{
		if ( !$this->isExists() )
			$sql = $this->getInsertSQL();
		else
			$sql = $this->getUpdateSQL();

		if ( !$sql )
			return 0;

		$this->getConnection()->query( $sql );
		$lastID = $this->getPrimaryValue();
		if (!$lastID)
			$lastID = $this->getConnection()->lastInsertId();

		if ( $lastID )
			$this->setPrimaryKeyValue( $lastID );

		$this->refreshFields();

		return $lastID;
	}

	/**
	 * @throws DBLayerException
	 *
	 * Удаляет запись из БД по указанному первичному ключу
	 */
	public function Delete ()
	{
		$sql = $this->getDeleteSql();

		if ( !$sql )
			throw new DBLayerException( "Empty query" );

		$this->getConnection()->query( $sql );
	}

	/**
	 * @return string
	 *
	 * Возвращает имя текущей таблицы
	 */
	public function getTable ()
	{
		return $this->tableName;
	}

	/**
	 * @return array
	 *
	 * Возвращает описанную модель текущей БД
	 */
	public function getMetaModel ()
	{
		return $this->_fields;
	}

	/**
	 * @return array
	 */
	public function getAdminMetaModel ()
	{
		$meta = array();
		foreach($this->getMetaModel() as $key => $value)
		{
			if (isset($value["SKIP_ADMIN"]) and $value["SKIP_ADMIN"] === true)
				continue;

			$meta[$key] = $value;
		}

		return $meta;
	}

    /**
     * @return array
     */
    public function getAdminMetaFields ()
    {
        return array_keys($this->getAdminMetaModel());
    }

	/**
	 * @return array
	 */
	public function getFieldsList ()
	{
		return array_keys( $this->_fields );
	}

	/**
	 * @param string $name Название поля
	 *
	 * @return null|mixed
	 * @throws DBLayerException
	 *
	 * Магический метод, позваляет получать значение поля модели как свойство
	 */
	public function getValue( $name )
	{
		if ( !isset( $this->_fields[ $name ] ) )
			throw new DBLayerException( "Field {$name} does not exist" );

		if ( isset( $this->_fields[ $name ][ "VALUE" ] ) )
			return $this->_fields[ $name ][ "VALUE" ];

		if ( isset( $this->_fields[ $name ][ "DEFAULT" ] ) )
			return $this->_fields[ $name ][ "DEFAULT" ];

		return null;
	}

	/**
	 * @param string $name  "Название поля"
	 * @param mixed $value "Значение поля"
	 *
	 * @throws DBLayerException
	 *
	 * Магический метод, позваляет записывать измнения в поля модели как в свойства
	 */
	public function setValue ( $name, $value )
	{
		if ( $this->isReadOnly( $name ) )
			throw new DBLayerException( "Field {$name} is ReadOnly mode!" );

		$this->writeFieldValue( $name, $value );
	}

	/**
	 * @param string $name
	 *
	 * @return bool
	 */
	public function __isset($name)
	{
		return isset( $this->_fields[ $name ] );
	}

	/**
	 * @param string $name Название поля
	 *
	 * @return null|mixed
	 *
	 * Магический метод, позваляет получать значение поля модели как свойство
	 */
	public function __get ( $name )
	{
		return $this->getValue( $name );
	}

	/**
	 * @param $name  Название поля
	 * @param $value Значение поля
	 *
	 * Магический метод, позваляет записывать измнения в поля модели как в свойства
	 */
	public function __set ( $name, $value )
	{
		$this->setValue( $name, $value );
	}

}