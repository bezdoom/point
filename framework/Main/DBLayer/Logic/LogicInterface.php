<?php

namespace Main\DBLayer\Logic;

interface LogicInterface
{
	/**
	 * @return string
	 */
	public function getString();
}