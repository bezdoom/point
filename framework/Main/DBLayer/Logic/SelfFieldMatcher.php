<?php

namespace Main\DBLayer\Logic;

class SelfFieldMatcher extends FieldMatcher
{
	public function __construct($fieldName, $logicMode, $fieldValue)
	{
		parent::__construct($fieldName, $logicMode, $fieldValue, null);
	}

	/**
	 * @return string
	 */
	public function getString()
	{
		return "{$this->fieldName} {$this->logicMode} {$this->fieldValue}";
	}
}