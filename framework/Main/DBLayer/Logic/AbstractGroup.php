<?php

namespace Main\DBLayer\Logic;

abstract class AbstractGroup implements LogicInterface
{
	/**
	 * @var string[]
	 */
	protected $elements = array();

	public function __construct()
	{
		foreach ( func_get_args() as $element )
		{
			if ( !($element instanceof LogicInterface) )
				continue;

			$this->add($element);
		}
	}

	/**
	 * @param LogicInterface $logic
	 */
	public function add(LogicInterface $logic)
	{
		$this->elements[] = $logic->getString();
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->getString();
	}
}