<?php

namespace Main\DBLayer\Logic;

use Main\DBLayer\DBLayerException;

class FieldMatcher implements LogicInterface
{
	const MODE_EQUAL = "=";
	const MODE_NOT_EQUAL = "!=";
	const MODE_GREAT = ">";
	const MODE_GREAT_OR_EQUAL = ">=";
	const MODE_LESS = "<";
	const MODE_LESS_OR_EQUAL = "<=";
	const MODE_LESS_OR_GREAT = "<>";
	const MODE_LIKE = "LIKE";
	const MODE_IN = "IN";
	const MODE_NOT_IN = "NOT IN";

	/**
	 * @var string
	 */
	protected $fieldName = null;
	/**
	 * @var string
	 */
	protected $logicMode = null;
	/**
	 * @var string
	 */
	protected $fieldValue = null;
	/**
	 * @var string
	 */
	protected $source = null;

	/**
	 * @param      $fieldName
	 * @param      $logicMode
	 * @param      $fieldValue
	 * @param null $source
	 */
	public function __construct($fieldName, $logicMode, $fieldValue, $source = null)
	{
		$this->source = $source;
		$this->fieldName = $fieldName;
		$this->logicMode = $logicMode;
		$this->fieldValue = $fieldValue;
	}

	/**
	 * @return string
	 * @throws \Main\DBLayer\DBLayerException
	 */
	public function getString()
	{
		if ($this->logicMode == self::MODE_IN || $this->logicMode == self::MODE_NOT_IN)
		{
			if (!is_array($this->fieldValue))
				throw new DBLayerException("Field {$this->fieldName} must be array (IN, NOT IN)");

			return "{$this->fieldName} {$this->logicMode} (" . implode(",", $this->fieldValue) . ")";
		}

		return "{$this->fieldName} {$this->logicMode} '{$this->fieldValue}'";
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->getString();
	}
}