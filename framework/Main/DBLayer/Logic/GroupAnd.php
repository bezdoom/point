<?php

namespace Main\DBLayer\Logic;

class GroupAnd extends AbstractGroup
{
	protected $logic = "AND";

	/**
	 * @return string
	 */
	protected function getLogic()
	{
		return $this->logic;
	}

	/**
	 * @return string
	 */
	public function getString()
	{
		$string = implode(" " . $this->getLogic() . " ", $this->elements);

		if (!trim($string))
			return "";

		return " (" . $string . ") ";
	}
}