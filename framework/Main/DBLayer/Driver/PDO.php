<?php

namespace Main\DBLayer\Driver;

class PDO extends \PDO
{
	function __construct($dsn, $username, $passwd, array $options = array())
	{
		parent::__construct($dsn, $username, $passwd, $options);
		$this->setAttribute( $this::ATTR_STATEMENT_CLASS, array( PDOStatement::class, array( $this ) ) );
	}
}