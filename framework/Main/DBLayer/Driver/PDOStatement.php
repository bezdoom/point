<?php

namespace Main\DBLayer\Driver;

class PDOStatement extends \PDOStatement
{
	protected function __construct()
	{
		$this->setFetchMode( PDO::FETCH_ASSOC );
	}

	/**
	 * @param string $modelName
	 */
	public function setFetchModel($modelName)
	{
		$this->setFetchMode(PDO::FETCH_CLASS, $modelName);
	}
}