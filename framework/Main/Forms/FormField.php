<?php

namespace Main\Forms;

abstract class FormField
{
    const CSRF_FIELD_NAME = "csrf-token";
    const BACK_URL_FIELD_NAME = "back-url";

    protected $name = null;
    protected $value = null;

    protected $required = false;

    /**
     * @var baseForm
     */
    protected $form = null;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @return $this
     */
    public function Required()
    {
        $this->required = true;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param baseForm $form
     */
    public function setForm(baseForm &$form)
    {
        $this->form = $form;
    }

    /**
     * @return baseForm
     */
    public function &getForm()
    {
        return $this->form;
    }

    public function readFieldFromRequest()
    {
        if (!$this->getForm())
            return $this;

        if (!$this->getName())
            return $this;

        $this->value = null;

        $request = $this->getForm()->getRequestArray();
        if (isset($request[$this->name]))
            $this->value = $request[$this->name];

        return $this;
    }

    /**
     * @return bool
     */
    public function Validate()
    {
        if (is_null($this->value))
            return false;

        return $this->value != false;
    }

    abstract public function Render(array $options = null);
}