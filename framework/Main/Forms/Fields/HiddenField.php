<?php

namespace Main\Forms\Fields;

class HiddenField extends InputField
{
    protected $type = "hidden";

    function __construct($name, $value = "", array $options = array())
    {
        $this->name = $name;
        $this->value = $value;
        $this->options = $options;
    }
}