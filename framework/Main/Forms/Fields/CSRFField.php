<?php

namespace Main\Forms\Fields;

use Kernel\System\Protection\CSRFToken;

class CSRFField extends HiddenField
{
    function __construct($name = self::CSRF_FIELD_NAME)
    {
        $this->Required();
        $this->name = $name;
        $this->value = CSRFToken::GetToken();
    }

    /**
     * @return bool
     */
    public function Validate()
    {
        if (is_null($this->value))
            return false;

        return CSRFToken::Validate($this->value);
    }
}