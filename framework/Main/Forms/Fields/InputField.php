<?php

namespace Main\Forms\Fields;

use Main\Forms\FormField;
use Main\HTML\Input;

class InputField extends FormField
{
    protected $type = null;
    /**
     * @var array
     */
    protected $options = array();

    function __construct($name, $value = "", $type = "text", array $options = array())
    {
        $this->name = $name;
        $this->value = $value;
        $this->type = $type;
        $this->options = $options;
    }

    public function setOptions(array $option = array())
    {
        $this->options = $option;
        return $this;
    }

    public function Render(array $options = null)
    {
        $options = is_array($options) ? array_merge($this->options, $options) : $this->options;
        return ( new Input($this->name, $this->value, $this->type) )->mergeProperties($options)->getHTML();
    }
}