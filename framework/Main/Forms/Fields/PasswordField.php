<?php

namespace Main\Forms\Fields;

class PasswordField extends InputField
{
    protected $type = "password";

    function __construct($name, $value = "", array $options = array())
    {
        $this->name = $name;
        $this->value = $value;
        $this->options = $options;
    }
}