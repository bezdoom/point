<?php

namespace Main\Forms\Fields;

class SubmitField extends InputField
{
    protected $type = "submit";

    function __construct($name = null, $value = null, array $options = array())
    {
        $this->name = $name;
        $this->value = $value;
        $this->options = $options;
    }
}