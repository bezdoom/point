<?php

namespace Main\Forms\Fields;

use Helper\String;

class BackUrlField extends HiddenField
{
    const DEFAULT_REDIRECT_URL = "/";
    const URL_PARSER_REGEXP = '#(http|https)://([A-z0-9_.\-]+)/(.*)#is';

    function __construct($name = self::BACK_URL_FIELD_NAME)
    {
        $this->Required();
        $this->name = $name;
        $this->value = $this->getBackUrl();
    }

    /**
     * @return string
     */
    protected function getBackUrl()
    {
        $referrer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : self::DEFAULT_REDIRECT_URL;

        return $this->validateUrl($referrer) ? $referrer : self::DEFAULT_REDIRECT_URL;
    }

    /**
     * @param string $url
     * @return bool
     */
    protected function validateUrl($url)
    {
        $lower = mb_strtolower($url, String::MBSTRING_CHARSET);

        if (String::StartWith("http://", $lower) or String::StartWith("https://", $lower))
            return false;

        if (!String::StartWith("/", $lower))
            return false;

        return true;
    }

    /**
     * @return bool
     */
    public function Validate()
    {
        if (is_null($this->value))
            return false;

        return $this->validateUrl($this->value);
    }
}