<?php

namespace Main\Forms\Fields;

class EmailField extends InputField
{
    protected $type = "email";

    function __construct($name, $value = "", array $options = array())
    {
        $this->name = $name;
        $this->value = $value;
        $this->options = $options;
    }

    /**
     * @return bool
     */
    public function Validate()
    {
        if (is_null($this->value))
            return false;

        return filter_var($this->value, FILTER_VALIDATE_EMAIL);
    }
}