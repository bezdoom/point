<?php

namespace Main\Forms;

use Helper\String;
use Kernel\System\PointRuntimeException;
use Main\HTML\Form;

abstract class baseForm
{
    protected $_method = "post";

    protected $_encode = null;
    protected $_action = null;

    /**
     * @var Form
     */
    protected $formTag = null;

    protected $isSubmit = null;
    protected $formResult = array();

    protected $options = array();

    /**
     * @var FormField[]
     */
    protected $_fields = null;

    protected $errorFields = array();

    /**
     * @param array $field
     * @return bool
     */
    protected function isValidField(array $field)
    {
        return isset($field['name']) and isset($field['type']);
    }

    /**
     * @param array $options
     * @return Form
     */
    protected function getFormTag(array $options = null)
    {
        if (!$this->formTag)
            $this->formTag = new Form($this->_action, $this->_method, $this->_encode);

        $this->formTag->mergeProperties(is_array($options) ? $options : $this->options);
        return $this->formTag;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @param FormField $field
     * @return $this
     * @throws \Kernel\System\PointRuntimeException
     */
    public function addField(FormField $field)
    {
        if (is_null($field))
            throw new PointRuntimeException("Form field can not be null");

        if (!is_array($this->_fields))
            $this->_fields = array();

        $key = $field->getName();
        if (!$key)
            $key = spl_object_hash($field);

        if (array_key_exists($key, $this->_fields))
            throw new PointRuntimeException("The key {$key} already exist in fields collection for this form");

        $field->setForm($this);
        $this->_fields[$key] = $field;

        return $this;
    }

    /**
     * @return FormField[]
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * @param string $key
     * @return FormField
     * @throws \Kernel\System\PointRuntimeException
     */
    public function getField($key)
    {
        if (!array_key_exists($key, $this->_fields))
            throw new PointRuntimeException("The key {$key} does not exist in fields collection for this form");

        return $this->_fields[$key];
    }

    /**
     * @return bool
     */
    public function isPostForm()
    {
        return mb_strtoupper($this->_method) == "POST";
    }

    /**
     * @return array
     */
    public function getRequestArray()
    {
        return $this->isPostForm() ? $_POST : $_GET;
    }

    /**
     * @param $fieldName
     * @return mixed
     */
    public function getResultValue($fieldName)
    {
        if (!$this->isSubmit())
            return null;

        if (!array_key_exists($fieldName, $this->formResult))
            return null;

        return $this->formResult[$fieldName];
    }

    /**
     * @return array
     */
    public function getFormResult()
    {
        if (!$this->isSubmit())
            return array();

        return $this->formResult;
    }

    /**
     * @return bool
     */
    public function isSubmit()
    {
        if (is_bool($this->isSubmit))
            return $this->isSubmit;

        $this->isSubmit = true;
        $request = $this->getRequestArray();

        foreach ($this->_fields as $field)
        {
            if (!$field->getName())
                continue;

            if (!isset($request[$field->getName()]))
            {
                $this->isSubmit = false;
                return $this->isSubmit;
            }
        }

        return $this->isSubmit;
    }

    public function Render()
    {
        $result = array();

        foreach ($this->_fields as $field)
            $result[] = $field->Render();

        return $this->beginForm() . "\n" . implode("\n", $result) . "\n"  . $this->endForm();
    }

    public function readFormSubmit()
    {
        if (!$this->isSubmit())
            return $this;

        $this->formResult = array();
        foreach ($this->_fields as $field)
        {
            $field->readFieldFromRequest();

            if (!$field->getName())
                continue;

            $this->formResult[$field->getName()] = $field->getValue();
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getErrorFields()
    {
        return $this->errorFields;
    }

    /**
     * @return bool
     */
    public function validateInputDate()
    {
        if (!$this->isSubmit())
            return false;

        $this->readFormSubmit();
        $this->errorFields = array();

        foreach ($this->_fields as $key => $field)
        {
            if (!$field->getName() or !$field->isRequired())
                continue;

            if (!$field->Validate())
                $this->errorFields[] = $key;
        }

        return empty($this->errorFields);
    }

    /**
     * @param array $options
     * @return string
     */
    public function beginForm(array $options = null)
    {
        return $this->getFormTag($options)->getOpenHTML();
    }

    /**
     * @return string
     */
    public function endForm()
    {
        return $this->getFormTag()->getCloseHTML();
    }
}