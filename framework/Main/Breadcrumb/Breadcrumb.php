<?php

namespace Main\Breadcrumb;

class Breadcrumb
{
    private static $instance = null;

    public static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }

    protected $breadcrumb = array();

    private function __clone() {}
    private function __construct() {}
}