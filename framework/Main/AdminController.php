<?php

namespace Main;

use Kernel\Admin\ActionButtonsGroup;
use Kernel\Admin\ModelTableGrid;
use Main\View\Buffer;

abstract class AdminController extends Controller
{
    /**
     * @var \Kernel\Admin\ActionButtonsGroup
     */
    protected $buttons = null;

    protected function initAdminTemplateVars()
    {
        Buffer::getInstance()->setTemplateVar("buttons", $this->buttons);
    }

    public function __construct()
    {
        parent::__construct();

        $this->buttons = ActionButtonsGroup::getInstance();
        $this->initAdminTemplateVars();
    }

    public function initAdminTableGrid($model,array $buttons = array())
    {
        $adminRender = (new ModelTableGrid($model))->setButtons($buttons)->Render();
        $this->setTemplateVar("tableGrid", $adminRender);
    }
}