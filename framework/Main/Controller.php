<?php

namespace Main;

use Main\View\Buffer;

abstract class Controller extends ContextLayer
{
	protected $defaultActionName = null;

	public function setTemplateVar($key, $value)
	{
		Buffer::getInstance()->setTemplateVar($key, $value);
	}

	public function unsetTemplateVar($key)
	{
		Buffer::getInstance()->unsetTemplateVar($key);
	}

	/**
	 * @return string
	 */
	public function getDefaultActionName()
	{
		return $this->defaultActionName;
	}
}