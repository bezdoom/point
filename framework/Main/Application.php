<?php

namespace Main;

use Helper\Storage;
use Kernel\System\MVC\ActionExecute;
use Main\Context\HttpApplication;
use Main\Routing\NotFoundException;
use Main\Routing\Route;
use Main\Routing\RouteMap;
use Main\View\Buffer;
use Main\View\IResponse;

abstract class Application
{
	protected $applicationName = null;
	protected $applicationVersion = null;

	/**
	 * @var RouteMap
	 */
	protected $routingMap = null;

	public function __construct()
	{
		if (!$this->applicationName)
			throw new \RuntimeException("Application name is null");

		if (!$this->applicationVersion)
			throw new \RuntimeException("Application version is null");

		HttpApplication::getInstance()->registerApp($this);
	}

	abstract protected function HttpApplicationInit(HttpApplication &$httpApp);

	public function handlerOnDenyAction()
	{
		header( 'Expires: Mon, 26 Jul 1970 05:00:00 GMT' );
		header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
		header( 'Cache-Control: no-store, no-cache, must-revalidate' );
		header( 'Cache-Control: post-check=0, pre-check=0', false );
		header( 'Pragma: no-cache' );
		header( 'location: /user/' );
		exit;
	}

	public function getRoutingMap()
	{
		return $this->routingMap;
	}

	public function getApplicationName()
	{
		return $this->applicationName;
	}

	public function getApplicationVersion()
	{
		return $this->applicationVersion;
	}

	public function registerRouteMap(RouteMap &$routeMap)
	{
		$this->routingMap = $routeMap;
		return $this;
	}

	public function registerControllerNamespace($namespace)
	{
		if (!$this->routingMap)
			throw new \RuntimeException("RouteMap is null");

		$this->routingMap->registerControllerNamespace($namespace);
		return $this;
	}

	public function registerDefaultControllerName($controller)
	{
		if (!$this->routingMap)
			throw new \RuntimeException("RouteMap is null");

		$this->routingMap->registerDefaultControllerName($controller);
		return $this;
	}

	public function registerTemplatePath($path)
	{
		Buffer::getInstance()->registerEnvPath($path);
		return $this;
	}

	public function registerNotFoundControllerName($controller)
	{
		if (!$this->routingMap)
			throw new \RuntimeException("RouteMap is null");

		$this->routingMap->registerNotFoundControllerName($controller);
		return $this;
	}

	public function registerRouting(array $map)
	{
		if (!$this->routingMap)
			throw new \RuntimeException("RouteMap is null");

		foreach($map as $regexp => $options)
			$this->routingMap->registerRoute(new Route($regexp, $options));

		return $this;
	}

	/**
	 * @param $className
	 *
	 * @return Controller
	 * @throws NotFoundException
	 */
	protected function initControllerObject($className)
	{
        if (!class_exists($className))
            throw new NotFoundException("Class {$className} does not exist");

		$class = new $className();

		if (!($class instanceof Controller))
			throw new NotFoundException("Bad controller");

		return $class;
	}

	protected function execute($className, $overrideActionByDefault = false)
	{
		$controller = $this->initControllerObject($className);
		if ($overrideActionByDefault)
			$actionName = $controller->getDefaultActionName();
		else
		{
			$actionName = $this->routingMap->getCurrentRoute()->getArguments()["action"];
			if (!$actionName)
				$actionName = $controller->getDefaultActionName();
		}

		return $this->doAction($controller, $actionName);
	}

	public function executeNotFound()
	{
		return $this->execute($this->routingMap->getNotFoundControllerClass(), true);
	}

	public function executeDefault()
	{
		return $this->execute($this->routingMap->getDefaultControllerClass(), true);
	}

	/**
	 * @param Controller $controller
	 * @param string     $actionName
	 * @param bool       $internal
	 *
	 * @return mixed
	 */
	protected function doAction(Controller &$controller, $actionName, $internal = false)
	{
		return (new ActionExecute($controller, $this, $actionName, $internal))->reflectionIt();
	}

	/**
	 * @return IResponse
	 */
	protected function handler()
	{
		if (Storage::getCurrentUri()->isRootDirectory())
			return $this->executeDefault();

		if (!$this->routingMap->getCurrentRoute())
			return $this->executeNotFound();

		try
		{
			return $this->execute($this->routingMap->getControllerClass());
		}
		catch (NotFoundException $e)
		{
			return $this->executeNotFound();
		}
	}

	public function start()
	{
		$this->HttpApplicationInit(HttpApplication::getInstance());

		$handler = $this->handler();
		if (!($handler instanceof IResponse))
			throw new \RuntimeException("Bad action result");

		print $handler->Render();
	}
}