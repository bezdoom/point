<?php

namespace Main\Context;

use Helper\Storage;

class HttpRequest
{
	private static $instance = null;

	public static function getInstance()
	{
		if (!self::$instance)
			self::$instance = new self();

		return self::$instance;
	}

	protected function __clone() {}
	protected function __construct() {}

	public function getGet($key)
	{
		if (!array_key_exists($key, $_GET))
			return null;

		return $_GET[$key];
	}

	public function getPost($key)
	{
		if (!array_key_exists($key, $_POST))
			return null;

		return $_POST[$key];
	}

    /**
     * @param $key
     * @return string
     */
    public function getServerVar($key)
    {
        if (!isset($_SERVER[$key]))
            return null;

        return $_SERVER[$key];
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return strtoupper($this->getServerVar("REQUEST_METHOD")) == "POST";
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        return $this->getServerVar('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
    }

    /**
     * @return string
     */
    public function getClientIp()
    {
        if (!empty($this->getServerVar('HTTP_CLIENT_IP')))
            return $this->getServerVar('HTTP_CLIENT_IP');

        if (!empty($this->getServerVar('HTTP_X_FORWARDED_FOR')))
            return $this->getServerVar('HTTP_X_FORWARDED_FOR');

        return $this->getServerVar('REMOTE_ADDR');
    }

    /**
     * @return \Helper\Uri
     */
    public function getCurrentUri()
    {
        return Storage::getCurrentUri();
    }
}