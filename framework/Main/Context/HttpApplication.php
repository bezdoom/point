<?php

namespace Main\Context;

use Helper\ArrayHelper;
use Helper\String;
use Kernel\System\PointRuntimeException;
use Main\Application;
use Main\HTML\Tag;

class HttpApplication
{
	const DEFAULT_ASSETS_NAMESPACE = "_main_";
	const SORT_DEFAULT = 100;

	private static $instance = null;

	public static function &getInstance()
	{
		if (!self::$instance)
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * @var Application
	 */
	public $app = null;
	protected $headCollection = array();

	protected $title = null;

	protected function __clone() {}
	protected function __construct() {}

	public function registerApp(Application &$app)
	{
		$this->app = $app;
	}

	/**
	 * @return string
	 * @throws PointRuntimeException
	 */
	public function getAppName()
	{
		if (!$this->app)
			throw new PointRuntimeException("Application not init");

		return $this->app->getApplicationName();
	}

	/**
	 * @return string
	 * @throws PointRuntimeException
	 */
	public function getAppVersion()
	{
		if (!$this->app)
			throw new PointRuntimeException("Application not init");

		return $this->app->getApplicationVersion();
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		if (!$this->title)
			return DEFAULT_TITLE;

		return $this->title;
	}

    /**
     * @param Tag $tag
     * @param int $sort
     * @param string $collection
     */
    public function addHeadTag(Tag $tag, $sort = null, $collection = self::DEFAULT_ASSETS_NAMESPACE)
	{
        if (!isset($this->headCollection[$collection]))
            $this->headCollection[$collection] = [];

		if (!is_numeric($sort))
        {
            $sort = self::SORT_DEFAULT;

            if (isset($this->headCollection[$collection]))
                $sort = (int) (count($this->headCollection[$collection]) * self::SORT_DEFAULT);
        }

		$this->headCollection[$collection][] = [$tag, $sort];
	}

	/**
	 * @param array $property
	 * @param int  $sort
     * @param string $collection
	 */
	public function addLink(array $property, $sort = null, $collection = self::DEFAULT_ASSETS_NAMESPACE)
	{
		$this->addHeadTag(
			(new Tag("link"))->setProperties($property),
			$sort, $collection
		);
	}

	/**
	 * @param string $name
	 * @param string $content
	 * @param int $sort
     * @param string $collection
	 */
	public function addMeta($name, $content, $sort = null, $collection = self::DEFAULT_ASSETS_NAMESPACE)
	{
		$this->addHeadTag(
			(new Tag("meta"))->setProperty("name", $name)->setProperty("content", $content),
			$sort, $collection
		);
	}

	/**
	 * @param string $src
	 * @param string $charset
	 * @param int $sort
     * @param string $collection
	 */
	public function addScript($src, $charset = null, $sort = null, $collection = self::DEFAULT_ASSETS_NAMESPACE)
	{
		$this->addHeadTag(
			(new Tag("script", true))->setProperty("src", $src)->setProperty("charset", $charset),
			$sort, $collection
		);
	}

    /**
     * @param string $href
     * @param string $media
     * @param int $sort
     * @param string $collection
     */
    public function addStyle($href, $media = "all", $sort = null, $collection = self::DEFAULT_ASSETS_NAMESPACE)
	{
		$this->addLink([
                        "rel" => "stylesheet",
                        "type" => "text/css",
                        "media" => $media,
                        "href" => $href
		               ], $sort, $collection);
	}

    /**
     * @param string $collection
     * @return string
     */
    public function getHeadAll($collection = self::DEFAULT_ASSETS_NAMESPACE)
	{
        if (!isset($this->headCollection[$collection]))
            return String::STRING_EMPTY;

		$head = array();
		$sortedCollection = ArrayHelper::Sort($this->headCollection[$collection], 1);

		foreach ($sortedCollection as $tagOption)
		{
			/**
			 * @var Tag $tag
			 */
			$tag = current($tagOption);
			$head[] = $tag->getHTML();
		}

		return implode("\n", $head);
	}
}