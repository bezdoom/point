<?php

namespace Main\Context;

class HttpResponse
{
    const HTTP_HEADER_403 = "HTTP/1.1 403 Forbidden";
    const HTTP_HEADER_404 = "HTTP/1.1 404 Not Found";
    const HTTP_HEADER_301_MOVED = "HTTP/1.1 301 Moved Permanently";
    const HTTP_HEADER_302_MOVED = "HTTP/1.1 302 Moved Temporarily";
    const HTTP_HEADER_302_FOUND = "HTTP/1.1 302 Found";
    const HTTP_HEADER_307_REDIRECT = "HTTP/1.1 307 Temporary Redirect";

	private static $instance = null;

	public static function getInstance()
	{
		if (!self::$instance)
			self::$instance = new self();

		return self::$instance;
	}

	protected function __clone() {}
	protected function __construct() {}

    /**
     * @param $string
     * @param $replace
     * @param $http_response_code
     *
     * @return void
     */
    public function setHeader($string, $replace = null, $http_response_code = null)
    {
        header($string, $replace, $http_response_code);
    }

    /**
     * @return void
     *
     * Выставляет в ответ заголовки, корректно выдающие JSON документ
     */
    public function setJsonHeaders()
    {
        $this->setNoCacheHeaders();
        $this->setHeader( 'Content-type: application/json; charset=utf-8' );
    }

    /**
     * @return void
     *
     * Выставляет в ответ заголовки, корректно выдающие plain-text документ
     */
    public function setPlainTextHeaders()
    {
        $this->setHeader( 'Content-type: text/plain' );
    }

    /**
     * @return void
     *
     * Выставляет в ответ заголовки, корректно выдающие XML документ
     */
    public function setXMLHeaders()
    {
        $this->setNoCacheHeaders();
        $this->setHeader( 'Content-type: application/xml' );
    }

    /**
     * @return void
     *
     * Выставляет в ответ заголовки, отключающие кеширование вывода в браузере
     */
    public function setNoCacheHeaders()
    {
        $this->setHeader( 'Expires: Mon, 26 Jul 1970 05:00:00 GMT' );
        $this->setHeader( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
        $this->setHeader( 'Cache-Control: no-store, no-cache, must-revalidate' );
        $this->setHeader( 'Cache-Control: post-check=0, pre-check=0', false );
        $this->setHeader( 'Pragma: no-cache' );
    }

    /**
     * @return void
     *
     * Посылает заголовки 404 ошибки для браузера
     */
    public function setNotFoundHeaders()
    {
        $this->setHeader(self::HTTP_HEADER_404);
    }
}