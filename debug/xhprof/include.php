<?php

require_once ('xhprof_lib/utils/xhprof_lib.php');
require_once ('xhprof_lib/utils/xhprof_runs.php');
// php.net/manual/ru/xhprof.constants.php
xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);

function xhprof_finish()
{
	$xhprof_data = xhprof_disable();
	$xhprof_runs = new XHProfRuns_Default();
	$run_id = $xhprof_runs->save_run($xhprof_data, "xhprof_testing");
	// Формируем ссылку на данные профайлинга и записываем ее в консоль
	$link = "http://" . $_SERVER['HTTP_HOST'] . "/.xhprof/index.php?run={$run_id}&source=xhprof_testing\n";
	print "<div><a href=\"{$link}\">{$link}</a></div>";
}

register_shutdown_function("xhprof_finish");