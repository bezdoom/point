<?php

namespace App;

use Main\Context\HttpApplication;
use Main\Routing\RouteMap;

class Application extends \Main\Application
{
	protected $applicationName = "Trends Brands";
	protected $applicationVersion = "1.0";

	public function __construct()
	{
		$this
			->registerTemplatePath(realpath(__DIR__ . "/View"))
			->registerRouteMap(RouteMap::getInstance())
			->registerControllerNamespace(__NAMESPACE__ . "\\" . "Controllers")
			->registerRouting($this->getRoutingArray())
			->registerDefaultControllerName("index")
			->registerNotFoundControllerName("notfound");

		parent::__construct();
	}

	protected function HttpApplicationInit(HttpApplication &$httpApp)
	{
		$httpApp->setTitle($this->applicationName);
		$httpApp->addScript("//code.jquery.com/jquery-1.10.2.min.js", null, 0);
	}

	/**
	 * @return array
	 */
	protected function getRoutingArray()
	{
		return [
			"#/([0-9A-z_-]+)(?:/+)([0-9A-z_-]*)(?:/*)#im" => [
				"controller" => "$1",
				"action" => "$2"
			]
		];
	}
}