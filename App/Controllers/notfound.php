<?php

namespace App\Controllers;

use Main\Controller;
use Main\View\View;

class notfound extends Controller
{
	protected $defaultActionName = "Error";

	/**
	 * @Action()
	 */
	public function Error()
	{
        $this->response->setNotFoundHeaders();
        $this->application->setTitle("404");

		return new View("_error/404.twig");
	}
}