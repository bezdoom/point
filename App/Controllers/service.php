<?php

namespace App\Controllers;

use Main\Controller;
use Main\View\Response;

class service extends Controller
{
	protected $defaultActionName = "phpinfo";

	/**
	 * @Action()
	 */
	public function phpinfo()
	{
		$this->setTemplateVar("info", phpinfo());
		return new Response("{{ info }}");
	}
}