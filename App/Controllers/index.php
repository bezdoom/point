<?php

namespace App\Controllers;

use Main\Controller;
use Main\View\View;

class index extends Controller
{
	protected $defaultActionName = "Index";

	/**
	 * @Action()
	 */
	public function Index()
	{
		return new View("index.twig");
	}
}