<?php

namespace App\Controllers;

use Main\Controller;
use Main\DBLayer\Finder;
use Main\View\View;
use PointCat\Models\CatalogSection;

class catalog extends Controller
{
	protected $defaultActionName = "Index";

    /**
     * @Action()
     */
    public function Validate()
    {
        $this->application->setTitle("Bad nested sets sections");

        $sqlStack = [
            "SELECT id FROM p_CatalogSections WHERE leftKey >= rightKey",
            "SELECT COUNT(id) as cnt, MIN(leftKey) as min, MAX(rightKey) as max FROM p_CatalogSections",
            //"SELECT id, ((`rightKey` - `leftKey`) / 2) AS `ostatok` FROM p_CatalogSections WHERE ostatok = 0",
            //"SELECT id, ((`leftKey` – `level` + 2) / 2) AS ostatok FROM p_CatalogSections WHERE ostatok = 1 ",
            "SELECT t1.id, COUNT(t1.id) AS rep, MAX(t3.rightKey) AS max_right FROM p_CatalogSections AS t1, p_CatalogSections AS t2, p_CatalogSections AS t3 WHERE t1.leftKey <> t2.leftKey AND t1.leftKey <> t2.rightKey AND t1.rightKey <> t2.leftKey AND t1.rightKey <> t2.rightKey GROUP BY t1.id HAVING max_right <> SQRT(4 * rep + 1) + 1 "
        ];

        foreach ($sqlStack as $iteration => $sql)
        {
            $row = $this->connection->getPdo()->query($sql);

            $this->setTemplateVar("list_{$iteration}", $row->fetchAll());
        }

        return new View("catalog/validate.twig");
    }

	/**
	 * @Action()
	 */
	public function Index()
	{
        if (isset($_GET['delete']) and isset($_GET['id']))
        {
            (new CatalogSection((int) $_GET['id']))->Delete();
            header("location: /catalog/");
            exit();
        }

        function make($parent = null)
        {
            $section = new \PointCat\Models\CatalogSection();
            $section->name = "It is a root, ".mt_rand(1000, 9999);
            if (intval($parent))
                $section->setParent(new \PointCat\Models\CatalogSection((int) $parent));

            return $section->Save();
        }

        function fill($parentId, $count)
        {
            $newId = 0;
            for($i = 1; $i<= $count; $i++)
                $newId = make($parentId);

            return $newId;
        }

        if (isset($_GET["make"]))
            make(isset($_GET["sid"]) ? (int) $_GET["sid"] : null);

        if (isset($_GET["fill"]))
        {
            $this->connection->getPdo()->query("TRUNCATE TABLE `p_CatalogSections` ");

            $id = make(); //init root level 1

            // level 2
            $id = fill($id, 3);

            // level 3
            $id = fill(2, 1);
            $id = fill(3, 3);
            $id = fill(4, 1);

            // level 4
            $id = fill(5, 2);
            $id = fill(7, 3);
            $id = fill(9, 2);
        }

		$finder =  Finder::Model(CatalogSection::class)
            ->order(["`leftKey` ASC"]);
			//->withCache(100)
			//->inPages(10);

        $this->setTemplateVar("list", $finder->find());

		return new View("catalog/section.twig");
	}
}