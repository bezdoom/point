<?php

namespace App\Controllers;

use Main\Controller;
use Main\User\AuthProvider\Direct;
use Main\View\Redirect;
use Main\View\View;
use App\Forms\User\AuthorizationForm;

class user extends Controller
{
	protected $defaultActionName = "Login";

	/**
	 * @Action()
	 */
	public function Login()
	{
		if ($this->user->isAuthorized())
			return new Redirect("/");

        $formLogin = new AuthorizationForm();
        $this->setTemplateVar("form", $formLogin);

        if (!$formLogin->validateInputDate())
            return new View("_user/login.twig");

        $email = $formLogin->getResultValue("email");
        $password = $formLogin->getResultValue("password");

        $backUrl = $this->request->getGet("back_url") ? $this->request->getGet("back_url") : "/";
        if ($this->user->Authorize(new Direct($email, $password)))
            return new Redirect($backUrl);

        return new View("_user/login.twig");
	}
}