<?php

namespace App\Forms\User;

use Main\Forms\baseForm;
use Main\Forms\Fields\CSRFField;
use Main\Forms\Fields\EmailField;
use Main\Forms\Fields\PasswordField;
use Main\Forms\Fields\SubmitField;

class AuthorizationForm extends baseForm
{
    function __construct()
    {
        $this->addField( (new CSRFField()) );
        $this->addField( (new EmailField("email"))->Required()->setOptions(array("placeholder" => "E-mail")) );
        $this->addField( (new PasswordField("password"))->Required()->setOptions(array("placeholder" => "Ваш пароль")) );
        $this->addField( (new SubmitField(null, "Авторизация")) );
    }
}