<?php

if (defined("CACHE_TTL_FINDER_SQL"))
    define("CACHE_TTL_FINDER_SQL", 900);

if (defined("DEFAULT_TITLE"))
    define("DEFAULT_TITLE", "Untitled");

if (defined("POINT_CSRF_MIXIN"))
    define("POINT_CSRF_MIXIN", md5("point-csrf-test"));

require_once "../framework/init.php";

// Register application
(new AutoLoader("App", ".."))->register();

// Register modules
foreach (require_once "modules.php" as $moduleNamespace => $moduleDirectory)
    (new AutoLoader($moduleNamespace, $moduleDirectory))->register();

// Register cache provider
Main\Cache\Manager::getInstance()->initProvider(
    APC_AVAILABLE === false ? new Main\Cache\Provider\APC() : new \Main\Cache\Provider\File()
);

// Configure mysql connection
require_once "dbconfig.php";
Main\DBLayer\ConnectionPool::setConnectionString($db_user, $db_password, $db_database, $db_host);

// Configure handlersocket connection
// (new AutoLoader("HSPHP", "../vendor/HSPHP-master/src"))->register();
// Main\DBLayer\HandlerSocket\HSPool::setConnectionSettings();