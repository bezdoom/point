<?php

namespace AdminApp;

use Main\Context\HttpApplication;
use Main\User\UserIdentify;

class Application extends \App\Application
{
	public function __construct()
	{
		parent::__construct();

		$this
			->registerTemplatePath(realpath(__DIR__ . "/View"))
			->registerControllerNamespace(__NAMESPACE__ . "\\" . "Controllers");
	}

	protected function HttpApplicationInit(HttpApplication &$httpApp)
	{
		$httpApp->setTitle($this->applicationName);

		$httpApp->addStyle("/assets/css/bootstrap.min.css");
		$httpApp->addStyle("/assets/css/bootstrap-theme.min.css");

		if (UserIdentify::getInstance()->isAdmin())
			$httpApp->addStyle("/assets/css/bootstrap-point-work.css");

		$httpApp->addScript("/assets/js/jquery/jquery.js");
		$httpApp->addScript("/assets/js/bootstrap.min.js");
	}

	protected function getRoutingArray()
	{
		return [
			"#/([0-9A-z_-]+)(?:/+)([0-9A-z_-]*)(?:/*)#im" => [
				"controller" => "$1",
				"action" => "$2"
			]
		];
	}
}