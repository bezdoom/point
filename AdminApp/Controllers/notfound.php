<?php

namespace AdminApp\Controllers;

use Main\AdminController;
use Main\View\View;

class notfound extends AdminController
{
	protected $defaultActionName = "Error";

	/**
	 * @Action()
	 */
	public function Error()
	{
        $this->response->setNotFoundHeaders();

        $this->application->setTitle("404");
		$this->application->addStyle("/assets/css/bootstrap-point-404.css");

		return new View("_error/404.twig");
	}
}