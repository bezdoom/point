<?php

namespace AdminApp\Controllers;

use Main\AdminController;
use Main\View\View;

class index extends AdminController
{
	protected $defaultActionName = "Index";

	/**
	 * @Action()
	 * @DenyFromAll()
	 */
	public function Index()
	{
		return new View("index.twig");
	}
}