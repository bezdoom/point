<?php

namespace AdminApp\Controllers;

use Kernel\Admin\TableGrid;
use Main\AdminController;
use Main\DBLayer\Finder;
use Main\DBLayer\Logic\FieldMatcher;
use Main\View\View;
use PointCat\Models\CatalogSection;

class catalog extends AdminController
{
	protected $defaultActionName = "Sections";

    /**
     * @Action()
     * @DenyFromAll()
     */
    public function Sections()
    {
        $finder = Finder::Model(CatalogSection::class)
            ->where(new FieldMatcher("level", FieldMatcher::MODE_EQUAL, 1));

        $this->setTemplateVar("tree", $finder->find()->getCollection());

        $this->application->setTitle("Секции");
        $this->initAdminTableGrid(CatalogSection::class);

        return new View("catalog/sections.twig");
    }
}