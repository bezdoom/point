<?php

namespace AdminApp\Controllers;

use Kernel\Admin\ActionButtons\DangerButton;
use Kernel\Admin\ActionButtons\SuccessButton;
use Kernel\Admin\TableGrid;
use Kernel\System\Protection\CSRFToken;
use Main\AdminController;
use Main\DBLayer\DBLayerException;
use Main\Models\Option;
use Main\Routing\NotFoundException;
use Main\User\AuthProvider\Direct;
use Main\View\Redirect;
use Main\View\View;

class options extends AdminController
{
	protected $defaultActionName = "Elements";

    /**
     * @Action()
     * @DenyFromAll()
     */
    public function Elements()
    {
        $this->application->setTitle("Опции");
        $this->initAdminTableGrid(Option::class, array("test"));
        $this->buttons->addButton(new SuccessButton("Добавить опцию", "/options/edit/"));

        return new View("_layout/list.twig");
    }

    /**
     * @return Redirect|View
     * @throws \Main\Routing\NotFoundException
     *
     * @Action()
     * @DenyFromAll()
     */
    public function Edit()
    {
        $primaryKey = $this->request->getGet("pk") ? $this->request->getGet("pk") : null;

        try
        {
            $element = new Option($primaryKey);
        }
        catch (DBLayerException $e)
        {
            throw new NotFoundException();
        }

        if (is_null($primaryKey))
            $this->application->setTitle("Создание опции");
        else
        {
            if ($this->request->getGet("delete"))
            {
                $element->Delete();
                return new Redirect("/options/");
            }

            $this->application->setTitle("Редактирование опции {$element->getPrimaryValue()}");
            $this->buttons->addButton(new DangerButton("Удалить опцию", "/options/edit/?" . http_build_query(array("pk" => $primaryKey, "delete" => true))));
        }

        $this->setTemplateVar("this", $element);
        $this->setTemplateVar("new", $primaryKey === null);
        $this->setTemplateVar("CSRFToken", CSRFToken::GetToken());

        if (!$this->request->isPost())
            return new View("options/edit.twig");

        if (!CSRFToken::Validate($this->request->getPost("csrf-token")))
            return new View("options/edit.twig", array("errorText" => "Поддельный ключ CSRF!"));

        if (is_null($primaryKey))
            $element->code = $this->request->getPost("code");

        $element->description = $this->request->getPost("description");
        $element->value = $this->request->getPost("value");
        $element->Save();

        return new Redirect("/options/");
    }
}