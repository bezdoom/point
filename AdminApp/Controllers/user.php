<?php

namespace AdminApp\Controllers;

use Kernel\Admin\TableGrid;
use Main\AdminController;
use Main\Forms\Fields\BackUrlField;
use Main\Models\UserGroup;
use Main\User\AuthProvider\Direct;
use Main\View\Redirect;
use Main\View\View;
use AdminApp\Forms\LoginForm;

class user extends AdminController
{
	protected $defaultActionName = "Login";

	/**
	 * @Action(GET, POST)
	 */
	public function Login()
	{
		if ($this->user->isAdmin())
			return new Redirect("/");

        $this->application->addStyle("/assets/css/bootstrap-point.css");

        $loginForm = (new LoginForm())->readFormSubmit();
        $this->setTemplateVar("form", $loginForm);

        if (!$loginForm->isSubmit())
		    return new View("_user/auth.twig");

        if (!$loginForm->validateInputDate())
        {
            $this->setTemplateVar("error", $loginForm->getErrorFields());
            return new View("_user/auth.twig");
        }

        $email = $loginForm->getResultValue("email");
        $password = $loginForm->getResultValue("password");
        $backUrl = $loginForm->getResultValue(BackUrlField::BACK_URL_FIELD_NAME);

        if ($this->user->Authorize(new Direct($email, $password)))
            return new Redirect($backUrl);

        $this->setTemplateVar("error", array("bad_user"));
        return new View("_user/auth.twig");

	}

	/**
	 * @Action()
	 * @return Redirect
	 */
	public function Logout()
	{
		$this->user->Logout();
		return new Redirect("/user/");
	}

	/**
	 * @Action()
	 * @DenyFromAll()
	 */
	public function Members()
	{
        $this->application->setTitle("Список пользователей");
        $this->initAdminTableGrid(\Main\Models\User::class);

		return new View("_layout/list.twig");
	}

    /**
     * @Action()
     * @DenyFromAll()
     */
    public function Groups()
    {
        $this->application->setTitle("Список групп пользователей");
        $this->initAdminTableGrid(UserGroup::class);

        return new View("_layout/list.twig");
    }
}