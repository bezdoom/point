<?php

namespace AdminApp\Forms;

use Main\Forms\baseForm;
use Main\Forms\Fields\BackUrlField;
use Main\Forms\Fields\CSRFField;
use Main\Forms\Fields\EmailField;
use Main\Forms\Fields\PasswordField;

class LoginForm extends baseForm
{
    function __construct()
    {
        $this->addField( (new CSRFField()) );
        $this->addField( (new BackUrlField()) );
        $this->addField( (new EmailField("email"))->Required()->setOptions(array("placeholder" => "E-Mail")) );
        $this->addField( (new PasswordField("password"))->Required()->setOptions(array("placeholder" => "Ваш пароль")) );
    }
}