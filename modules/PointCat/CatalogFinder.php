<?php

namespace PointCat;

use Main\DBLayer\Finder;
use PointCat\Models\CatalogSection;

class CatalogFinder
{
    /**
     * @param int $productID
     * @return CatalogSection[]
     */
    static public function getSectionForProduct($productID)
    {
        $productID = (int) $productID;

        $sql = "SELECT {=fields}
                FROM `p_CatalogProductSection` as `m`
                INNER JOIN `{=table}` as `s` ON `s`.`id` = `m`.`sectionId`
                WHERE `s`.`active` = 'Y' AND `m`.`productId` = '{$productID}'";

        return Finder::Model(CatalogSection::class)->find($sql)->getCollection();
    }

    static public function getSectionTree()
    {
        $finder =  Finder::Model(CatalogSection::class)
            ->order(["`leftKey` ASC"]);

        $collection = $finder->find()->getCollection();

        $trees = [];
        if (count($collection) > 0)
        {
            $stack = [];

            foreach ($collection as $section)
            {
                /**
                 * @var CatalogSection $section
                 */


            }
        }

        return $trees;
    }
}