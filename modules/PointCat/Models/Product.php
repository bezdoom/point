<?php

namespace PointCat\Models;

use Main\DBLayer\baseLayer;
use PointCat\CatalogFinder;

class Product extends baseLayer
{
	protected $tableName = "p_CatalogProduct";

	protected $_fields = [
		"id" => [
			"TYPE" => "INT",
			"AUTO_INCREMENT" => true,
			"KEY" => "PRIMARY",
            "TITLE" => "ID"
		],
		"active" => [
			"TYPE" => "ENUM('Y','N')",
			"DEFAULT" => "Y",
            "TITLE" => "Активность"
		],
		"name" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 255,
            "TITLE" => "Название"
		],
        "created_at" => [
            "TYPE" => "DATETIME",
            "TITLE" => "Дата создания"
        ],
        "update_at" => [
            "TYPE" => "DATETIME",
            "TITLE" => "Дата изменения"
        ]
	];

    /**
     * @return CatalogSection[]
     */
    public function getSections()
    {
        if (!$this->getPrimaryValue())
            return [];

        return CatalogFinder::getSectionForProduct($this->getPrimaryValue());
    }
}