<?php

namespace PointCat\Models;

use Main\DBLayer\baseLayer;
use Main\DBLayer\Finder;
use Main\DBLayer\Logic\FieldMatcher;
use Main\DBLayer\Logic\GroupAnd;

class CatalogSection extends baseLayer
{
	protected $tableName = "p_CatalogSections";

    /**
     * @var self
     */
    protected $parent = null;
    /**
     * @var self[]
     */
    protected $children = null;

	protected $_fields = [
		"id" => [
			"TYPE" => "INT",
			"AUTO_INCREMENT" => true,
			"KEY" => "PRIMARY",
            "TITLE" => "ID"
		],
		"active" => [
			"TYPE" => "ENUM('Y','N')",
			"DEFAULT" => "Y",
            "TITLE" => "Активность"
		],
        "code" => [
            "TYPE" => "VARCHAR",
            "LENGTH" => 150,
            "KEY" => "UNIQUE",
            "TITLE" => "Символьный код"
        ],
        "level" => [
            "TYPE" => "INT",
            "KEY" => "INDEX",
            "TITLE" => "Глубина вложенности",
            "DEFAULT" => 0,
            "SKIP_ADMIN" => true,
            "READ_ONLY" => true
        ],
        "leftKey" => [
            "TYPE" => "INT",
            "KEY" => "INDEX",
            "TITLE" => "Левая граница",
            "DEFAULT" => 0,
            "SKIP_ADMIN" => true,
            "READ_ONLY" => true
        ],
        "rightKey" => [
            "TYPE" => "INT",
            "KEY" => "INDEX",
            "TITLE" => "Правая граница",
            "DEFAULT" => 0,
            "SKIP_ADMIN" => true,
            "READ_ONLY" => true
        ],
		"name" => [
			"TYPE" => "VARCHAR",
			"LENGTH" => 255,
            "TITLE" => "Название"
		]
	];

    /**
     * @return int
     */
    public function getLevel()
    {
        return (int) $this->level <= 0 ? 0 : (int) $this->level;
    }

    /**
     * @return int
     */
    public function getRightKey()
    {
        return (int) $this->rightKey <= 0 ? 0 : (int) $this->rightKey;
    }

    /**
     * @return int
     */
    public function getLeftKey()
    {
        return (int) $this->leftKey <= 0 ? 0 : (int) $this->leftKey;
    }

    /**
     * @return int
     */
    protected function getInsertRightKey()
    {
        if (is_null($this->parent))
            return $this->getMaxRightKey() + 1;

        return $this->parent->getRightKey();
    }

    /**
     * @return bool
     *
     * Обновляем ключи существующего дерева, узлы стоящие за родительским узлом +
     * Обновляем родительскую ветку
     */
    protected function updateExistKeysBeforeInsert()
    {
        $rightKey = $this->getInsertRightKey();
        if ($rightKey < 0)
            return false;

        $sql = "UPDATE `{$this->getTable()}`
                SET
                    `rightKey` = `rightKey` + 2,
                    `leftKey` = IF(`leftKey` > {$rightKey}, `leftKey` + 2, `leftKey`)
                WHERE `rightKey` >= {$rightKey}";
        $this->getConnection()->query($sql);

        return true;
    }

    /**
     * @return bool
     *
     * Обновление ключей родительской ветки и обновление ключей узлов, стоящих за родительской веткой.
     * Следует правда учесть, что обновление будет производиться в другом порядке,
     * так как ключи у нас уменьшаются.
     */
    protected function updateExistKeysAfterDelete()
    {
        $rightKey = $this->getRightKey();
        $leftKey = $this->getLeftKey();

        $sql = "UPDATE `{$this->getTable()}`
                SET
                    `leftKey` = IF(`leftKey` > {$leftKey}, `leftKey` - ({$rightKey} - {$leftKey} + 1), `leftKey`),
                    `rightKey` = `rightKey` - ({$rightKey}  - {$leftKey} + 1)
                WHERE `rightKey` > {$rightKey}  ";
        $this->getConnection()->query($sql);

        return true;
    }

    /**
     * @return int
     */
    protected function getMaxRightKey()
    {
        $sql = "SELECT MAX(`rightKey`) as `max` FROM `{$this->getTable()}`";
        $state = $this->getConnection()->query($sql);

        if (!$state)
            return 0;

        if ($result = $state->fetch())
            return (int) $result["max"];

        return 0;
    }

    /**
     * @return $this
     */
    protected function getParent()
    {
        if (is_null($this->parent))
            return $this;

        return $this->parent;
    }

    /**
     * @return void
     */
    protected function generateCode()
    {
        if (!$this->code)
            $this->code = md5(uniqid(microtime(), true) . mt_rand(100000, 999999));
    }

    /**
     * @return void
     */
    protected function beforeInsertPrepare()
    {
        $rightKey = $this->getInsertRightKey();
        $level = $this->getParent()->getLevel() + 1;

        $this->updateExistKeysBeforeInsert();

        $this->writeFieldValue("level", $level);
        $this->writeFieldValue("leftKey", $rightKey);
        $this->writeFieldValue("rightKey", $rightKey + 1);
    }

    /**
     * @return self
     */
    protected function deleteTree()
    {
        $sql = "DELETE FROM `{$this->getTable()}`
                WHERE `leftKey` >= {$this->getLeftKey()} AND `rightKey` <= {$this->getRightKey()} ";
        $this->getConnection()->query($sql);

        return $this;
    }

    /**
     * @return self[]
     */
    public function getChildren()
    {
        if (!$this->isExists())
            $this->children = [];

        if (is_array($this->children))
            return $this->children;

        $finder = Finder::Model(self::class)->where(
            new GroupAnd(
                new FieldMatcher("leftKey", FieldMatcher::MODE_GREAT, $this->getLeftKey()),
                new FieldMatcher("rightKey", FieldMatcher::MODE_LESS, $this->getRightKey()),
                new FieldMatcher("level", FieldMatcher::MODE_EQUAL, $this->getLevel() + 1)
            )
        )->order(["`leftKey` ASC"]);

        $this->children = $finder->find()->getCollection();

        return $this->children;
    }

    /**
     * @param CatalogSection $parent
     *
     * @return $this
     */
    public function setParent(CatalogSection $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return void
     */
    public function Delete()
    {
        $this->deleteTree()->updateExistKeysAfterDelete();
    }

    /**
     * @return int
     */
    public function Save()
    {
        $this->generateCode();

        if (!$this->isExists())
            $this->beforeInsertPrepare();

        return parent::Save();
    }
}